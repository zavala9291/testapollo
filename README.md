> This is a test project for student management

# ApolloTest
- Test application that allows users to create an account to setup groups. Afterwards, schools and students could be created. Everything created and stored in the app is backed up by using Firebase Realtime Database.

# Requirements

- iOS 11.0+
- Xcode 10.0+
- Swift 3.0+
- Firebase Account

# Installation

## Firebase

[Firebase](https://firebase.google.com) is a dependency for this project. You could setup a free account by signing in with your gmail and adding a project. 

- After creating the project, add an ios app to the project, follow instructions to register app and include configuration file into Xcode project.

- The configuration .plist is needed in order for the app to connect to your firebase account to then be able to authenticate users and store data

- Once GoogleService-Info.plist is imported into the project, the firebase database, storage and authentication sections must be configured within Firebase to be able to run the test project without issues. 

### Authentication

- Authentication is used in Firebase in order to be able to create users and to verify the email account used to register in the app.

- In the Firebase console ( https://console.firebase.google.com/u/0/ ) select your project and select Authentication from the Develop menu.

- Select "Setup sign in method", choose the email/password option and enable the “allow users to sign up using their email address and password…” option

### Database

- Realtime Database is used to backup all data created in the app, with the exception of any pictures taken or added to students.

- In the Firebase console ( https://console.firebase.google.com/u/0/ ) select your project and select Database from the Develop menu, make sure that the Realtime Database is selected.

- Select "Create a database", select "Start in test mode"

Make sure the following rules are applied in the Realtime Database to ensure there are no issues writing or reading from the database. 

{
    "rules": {
        ".read": "auth != null",
        ".write": "auth != null"
        }
}

### Storage 

- Storage is used in Firebase to be able to upload any images you want to assign to students to later retrieve them if necessary.

- In the Firebase console ( https://console.firebase.google.com/u/0/ ) select your project and select Storage from the Develop menu.

- Select "Get Started" and accept the default rules.

## CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

> CocoaPods 1.1.0+ is required to build ApolloTest

Then, run the following command:

```bash
$ pod install
```

# Author 

- Ismael Zavala - iOS Developer 

- Linkedin profile - https://www.linkedin.com/in/ismael-zavala-42b4278a/

- Any issues or questions feel free to contact me at ismaeliosdeveloper@gmail.com

# License 

This project is licensed under the MIT License - see the LICENSE file for details



