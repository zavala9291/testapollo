//
//  AttendanceHistoryDetailVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/18/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class AttendanceHistoryDetailVC: UITableViewController {
    
    var students: [Student]?
    var schoolDays: [SchoolDay]?
    fileprivate let realm = try! Realm()
    let manager = OnlineDatabaseManager()
    var onlineSchoolDayID = ""
    
    override func viewDidLoad() {
        //setup
        
        guard !onlineSchoolDayID.isEmpty else {
            return
        }
        getLocalHistory()
        getStudents(checkInId: onlineSchoolDayID)
    }
    
    func getStudents(checkInId: String) {
        weak var weakSelf = self
        manager.getHistoryOfDate(id: onlineSchoolDayID) { (completed) in
            //what next?
            weakSelf?.getLocalHistory()
        }
    }
    
    func getLocalHistory() {
        let attendancePredicate = NSPredicate(format: "id == %@", onlineSchoolDayID)
        self.schoolDays = self.realm.objects(SchoolDay.self).filter(attendancePredicate).toArray(ofType: SchoolDay.self)
        self.tableView.reloadData()
    }
    
    //Mark: Tableview
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        guard let day = self.schoolDays?[indexPath.row] else {
            return UITableViewCell()
        }
        
        cell.textLabel?.text = day.studentName
        cell.accessoryType = day.present ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolDays?.count ?? 0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
