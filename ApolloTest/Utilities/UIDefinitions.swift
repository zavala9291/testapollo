//
//  UIDefinitions.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/12/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import Foundation

//AllDatabaseInfo
open class UIDefinitions: NSObject {
    static let allDatabaseInfo = "AllDatabaseInfo"
    static let owner = "owner"
}

//AllDatabases
@objc extension UIDefinitions {
    static let allDatabases = "AllDatabases"
    static let allCheckIns = "AllCheckIns"
    static let isPresent = "isPresent"
    static let allSchools = "AllSchools"
    static let allStudents = "AllStudents"
    static let AllPictures = "AllPictures"
    static let schoolName = "schoolName"
    static let allUsers = "AllUsers"
    static let databaseInfo = "databaseInfo"
    static let databaseId = "databaseId"
    static let databaseName = "databaseName"
    static let AllStudentsCheckIns = "AllStudentsCheckIns"
    static let AllNotes = "AllNotes"
    static let note = "note"
    
    static let schoolId = "schoolId"
    
    //date values
    static let date = "date"
}

//AllUsers
@objc extension UIDefinitions {
    static let firstName = "firstName"
    static let lastName = "lastName"
}

// MARK: common
@objc extension UIDefinitions {
    static let id = "id"
    static let name = "name"
    static let mainPhotoId = "mainPhotoId"
    static let users = "users"
    static let approved = "approved"
    static let role = "role"
}
