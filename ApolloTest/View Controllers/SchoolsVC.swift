//
//  SchoolsVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/1/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//
import UIKit
import Firebase
import RealmSwift
import SCLAlertView

class SchoolsVC: UITableViewController {
    
    var ref: DatabaseReference!
    var allSchools: [School]?
    let realm = try! Realm()
    var currentSelectedSchoolId: String?
    let manager = OnlineDatabaseManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        self.tableView.backgroundColor = UIColor.lightGray
        self.tableView.separatorStyle = .singleLine
        self.title = "Select School"
        allSchools = []
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(SchoolsVC.refresh(sender:)), for: UIControlEvents.valueChanged)
        

        let addSchoolButton = UIBarButtonItem(image: UIImage(named: "add"), style: .plain, target: self, action: #selector(SchoolsVC.addSchoolAction))
        navigationItem.rightBarButtonItem = addSchoolButton
        
        guard let currentSchoolId = realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get current settings")
            return
        }
        
        currentSelectedSchoolId = currentSchoolId
        
        if let schoolId = currentSelectedSchoolId, !schoolId.isEmpty {
            //get selected school
            let settingsResults = realm.objects(SelectedSettings.self)
            if let currentSettings = settingsResults.first {
                currentSelectedSchoolId = currentSettings.selectedSchoolId
            }
        }
        
        self.syncAllSchools()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.syncAllSchools()
    }
    
    func syncAllSchools() {
        weak var weakSelf = self
        manager.syncSchoolList { (completed) in
            weakSelf?.getSchools()
        }
    }
    
    func getSchools() {
        self.allSchools?.removeAll()
        
        let fetchedSchools = self.realm.objects(School.self)
        for school in fetchedSchools {
            allSchools?.append(school)
        }
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }

    func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.syncAllSchools()
    }
    
    func addSchoolAction() {
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: false
        )
        let alert = SCLAlertView(appearance: appearance)
        let txt = alert.addTextField("Enter new school name")
        alert.addButton("Create School") {
            if let newSchoolName = txt.text {
                //create new school
                let newID = UUID.init().uuidString
                
                let onlineManager = OnlineDatabaseManager()
                weak var weakSelf = self
                DispatchQueue.main.async {
                    weakSelf?.ref.child(UIDefinitions.allDatabases).child(onlineManager.dbId).child(UIDefinitions.allSchools).child(newID).setValue([UIDefinitions.schoolName:newSchoolName])
                    
                    //sync schools after this
                    self.syncAllSchools()
                }
                
                
            }
        }
        alert.addButton("Cancel") {
            //do nothing
        }
        
        alert.showEdit("New School", subTitle: "Enter a new school name to add a new school")
    }
    
    // MARK - UITableview Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
        }
        
        guard let selectedSchool = allSchools?[indexPath.row] else {
            return
        }
        
        guard let selectedSchoolName = selectedSchool.name else {
            return
        }
        
        guard let selectedSchoolId = selectedSchool.id else {
            return
        }
        
        let oldSettings = realm.objects(SelectedSettings.self)
        try! self.realm.write {
            self.realm.delete(oldSettings)
        }
        
        currentSelectedSchoolId = selectedSchoolId
        let selectedSettings = SelectedSettings(selectedSchoolId: selectedSchoolId)
        try! self.realm.write {
            self.realm.add(selectedSettings)
        }
        
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell.init()
        
        guard let schoolSelected = allSchools?[indexPath.row],
            let currentId = schoolSelected.id else {
            return cell
        }
        
        cell.textLabel?.text = schoolSelected.name
        if currentId == currentSelectedSchoolId {
            cell.accessoryType = .checkmark
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allSchools?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
