//
//  HomeVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 9/16/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import RealmSwift

internal protocol HomeVCDelegate: class {
    func selectedRow(_homeVC: HomeVC, rowSelected: Int)
    func signedOut(_homeVC: HomeVC)
}

class HomeVC: UITableViewController {
    
    weak var delegate: HomeVCDelegate?
    
    fileprivate var ref: DatabaseReference!
    var isUserActive = false
    var databaseId: String?
    var manager = OnlineDatabaseManager()
    fileprivate let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        manager.getDatabaseId { (dbId) in
            self.databaseId = dbId
        }
        
        ref = Database.database().reference()
        
        log.debug("realm is: \(String(describing: Realm.Configuration.defaultConfiguration.fileURL))")
        
        self.tableView.backgroundColor = UIColor.lightGray
        self.tableView.register(R.nib.homeCell)
        self.tableView.separatorStyle = .singleLine
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sign Out", style: .plain, target: self, action: #selector(signOutWarning))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(settingsAction))
    }
    
    func isUserAuthorized(completion: @escaping (_ id: Bool) -> Void) {
        weak var weakSelf = self
        
        Auth.auth().currentUser?.reload(completion: { (error) in
            
            guard let user = Auth.auth().currentUser else {
                completion(false)
                return
            }
            
            guard user.isEmailVerified else {
                completion(false)
                user.sendEmailVerification(completion: nil)
                return
            }
            
            weakSelf?.manager.getCurrentOnlineUser { (completed, approved) in
                if completed {
                    completion(approved)
                } else {
                    weakSelf?.manager.getDatabaseId(completion: { (id) in
                        weakSelf?.databaseId = id
                        
                        if let userInfo = self.realm.objects(User.self).first {
                            completion(userInfo.approved)
                            return
                        } else {
                            completion(false)
                            return
                        }
                    })
                }
            }
        })
    }
    
    func settingsAction() {
        if let settingsVC = R.storyboard.main.settingsVC() {
            navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    
    func signOutWarning() {
        let alert = UIAlertController.init(title: "Sign Out?", message: "You will be signed out.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Sign Out", style: .destructive, handler: { (action) in
            self.signOutAction()
        }))
        self.present(alert, animated: false, completion: nil)
    }
    
    func signOutAction() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            log.error("Error signing out: \(signOutError)")
        }
        
        delegate?.signedOut(_homeVC: self)
    }
    
    // MARK - enums
    public enum HomeSelections: Int {
        case listOfSchools
        case listOfStudents
        case checkInStudents
        case attendanceHistory
        case listOfUsers
        case homeSelectionsCount
        
        func localizedString() -> String {
            switch self {
            case .listOfSchools:
                return NSLocalizedString("List of schools", comment: "List of schools")
            case .listOfStudents:
                return NSLocalizedString("List of students", comment: "listOfStudents")
            case .checkInStudents:
                return NSLocalizedString("Check In Students", comment: "Check In Students")
            case .homeSelectionsCount:
                return NSLocalizedString("error getting cell title", comment: "error")
            case .attendanceHistory:
                return NSLocalizedString("Attendance History", comment: "Attendance History")
            case .listOfUsers:
                return NSLocalizedString("List of users", comment: "List of users")
            }
        }
    }
    
    // MARK - UITableview Methods
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.homeCell.identifier, for: indexPath) as? HomeCell else {
            log.error("failed to initialize home cell")
            return UITableViewCell()
        }
        
        guard let cellType = HomeSelections(rawValue:indexPath.row) else {
            log.error("getting cell type")
            return UITableViewCell()
        }
        cell.configureCell(title:cellType.localizedString())
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HomeSelections.homeSelectionsCount.rawValue
    }
    
    func notYetImplemented() {
        let alert = UIAlertController.init(title: "Empty for now", message: "Not yet implemented.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: false, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        weak var weakSelf = self
        isUserAuthorized { (authorized) in
            
            if !authorized {
                
                guard let currentlyVerified = Auth.auth().currentUser?.isEmailVerified else {
                    weakSelf?.showAlert(message: "Verify by opening link in email sent to your login email address.", title: "Email not yet verified.")
                    return
                }
                
                if !currentlyVerified {
                    weakSelf?.showAlert(message: "Verify by opening link in email sent to your login email address.", title: "Email not yet verified.")
                } else {
                    weakSelf?.showAlert(message: "Please contact an admin to activate your account.", title: "Account not yet active.")
                }
                return
            } else {
                self.delegate?.selectedRow(_homeVC: self, rowSelected: indexPath.row)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func showAlert(message: String, title: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        let action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        
        self.present(alert, animated: false, completion: nil)
    }
}
