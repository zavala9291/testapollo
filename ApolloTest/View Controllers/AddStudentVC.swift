//
//  AddStudentVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/2/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import Firebase
import UIKit

class AddStudentVC: UIViewController {
    
    @IBOutlet var addButton: UIButton!
    @IBOutlet var studentName: UITextField!
    @IBOutlet var studentNumber: UITextField!
    
    var currentSchoolId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add New Student"
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        //check that name isn't empty
        guard let name = studentName.text else {
            log.error("no name")
            return
        }
        
        let onlineDatabase = OnlineDatabaseManager()
        onlineDatabase.addStudent(name: name)
        
        self.navigationController?.popViewController(animated: false)
        
    }
}
