//
//  AttendanceHistoryVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 11/14/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import CVCalendar

class AttendanceHistoryVC: UIViewController, CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    
    @IBOutlet var controllerCalendarMenu: CVCalendarMenuView!
    @IBOutlet var controllerCalendar: CVCalendarView!
    var ref: DatabaseReference!
    let realm = try! Realm()
    let manager = OnlineDatabaseManager()
    
    override func viewDidLoad() {
        self.title = "Please select a date"
        self.controllerCalendar.delegate = self
        self.controllerCalendarMenu.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Show", style: .plain, target: self, action: #selector(viewHistory))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        controllerCalendar.commitCalendarViewUpdate()
        controllerCalendarMenu.commitMenuViewUpdate()
    }
    
    func viewHistory() {
        var dateComponents = DateComponents()
        dateComponents.day = self.controllerCalendar.presentedDate.day
        dateComponents.month = self.controllerCalendar.presentedDate.month
        dateComponents.year = self.controllerCalendar.presentedDate.year
        
        let userCalendar = Calendar.current // user calendar
        guard let dateToLookup = userCalendar.date(from: dateComponents) else {
            //error setting up date to lookup
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        //id of school day will be "Nov 4, 2017"
        let dateString = dateFormatter.string(from: dateToLookup)
        
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            return
        }
        
        let onlineDatabaseId = "\(dateString)-\(schoolID)"
        
        guard let viewController = R.storyboard.main.attendanceHistoryDetailVC() else {
            return
        }
        viewController.onlineSchoolDayID = onlineDatabaseId
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    //MARK: calendarView Delegate
    func presentationMode() -> CalendarMode {
        return CalendarMode.monthView
    }
    
    func firstWeekday() -> Weekday {
        return Weekday.monday
    }
}
