//
//  ListOfPicturesVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 8/3/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

enum PhotoObject: Int {
    case school
    case student
}

class ListOfPicturesVC: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var emptyTextView: UITextView!
    
    var student: Student?
    let realm = try! Realm()
    var photosOfStudent: [Photo]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPicture))
        
        //collection view setup
        self.collectionView.register(R.nib.photoCell)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let currentStudent = student else {
            log.error("unable to get student")
            return
        }
        
        let online = OnlineDatabaseManager()
        online.getStudentPictures(idOfStudent: currentStudent.id) { (completed) in
            log.debug("finished running getting pictures of students")
            DispatchQueue.main.async {
                self.reloadPictures()
            }
        }
    }
    
    func addPicture() {
        let vc = R.storyboard.main.addPictureVC()
        guard let viewController = vc else {
            return
        }
        viewController.student = self.student
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func reloadPictures() {
        
        guard let currentStudentID = student?.id else {
            log.error("unable to get student")
            return
        }
        
        let predicate = NSPredicate(format: "studentId == %@", currentStudentID)
        self.photosOfStudent = self.realm.objects(Photo.self).filter(predicate).toArray(ofType: Photo.self)
        self.collectionView.reloadData()
        
        self.emptyTextView.isHidden = self.photosOfStudent?.count != 0
        
    }
}


//MARK: CollectionView
extension ListOfPicturesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let photos = self.photosOfStudent else {
            return 0
        }
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.photoCell.identifier, for: indexPath) as? PhotoCell else {
            log.error("failed to initialize photo cell")
            return UICollectionViewCell()
        }
        
        guard let photo = photosOfStudent?[indexPath.row] else {
            return UICollectionViewCell()
        }

        cell.configure(photo: photo, selected: true)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //enable / dissable selection
    }
    
}
