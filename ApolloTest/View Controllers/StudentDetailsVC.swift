//
//  StudentDetailsVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/26/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import UIKit

class StudentDetailsVC: UITableViewController {
    
    var student: Student?
    
    fileprivate enum StudentDetails: Int {
        case listOfNotes
        case photoGallery
        case count
        
        func localizedString() -> String {
            switch self {
            case .listOfNotes:
                return NSLocalizedString("List of notes", comment: "List of notes")
            case .photoGallery:
                return NSLocalizedString("Photo Gallery", comment: "Photo Gallery")
            case .count:
                return NSLocalizedString("StudentDetailCount", comment: "StudentDetailCount")
            }
        }
    }
    
    override func viewDidLoad() {
       
    }
    
    // MARK - UITableview Methods
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        guard let cellType = StudentDetails(rawValue:indexPath.row) else {
            log.error("getting cell type")
            return UITableViewCell()
        }
        
        cell.textLabel?.text = cellType.localizedString()
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StudentDetails.count.rawValue
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sendUserToAppropriateScreen(rowSelected: indexPath.row)
    }
    
    func sendUserToAppropriateScreen(rowSelected: Int) {
        var viewController: UIViewController?
        
        switch rowSelected {
        case StudentDetails.listOfNotes.rawValue:
            guard let notesViewController = R.storyboard.main.listOfNotesVC() else {
                log.error("unable to initialize list of notes vc")
                return
            }
            notesViewController.student = self.student
            navigationController?.pushViewController(notesViewController, animated: true)
            return
            
            
        case StudentDetails.photoGallery.rawValue:
//            viewController = R.storyboard.main.addPictureVC()
//            viewController = R.storyboard.main.listOfPicturesVC()
            guard let photosViewController = R.storyboard.main.listOfPicturesVC() else {
                log.error("unable to initialize list of pictures vc")
                return
            }
            photosViewController.student = self.student
            navigationController?.pushViewController(photosViewController, animated: true)
            return
            
        default:
            log.error("unable to navigate user")
            return
        }
        
        guard let vc = viewController else {
            log.error("unable to initialized view controller")
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}
