//
//  AddNoteVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 5/4/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import RealmSwift

class AddNoteVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet var noteTextView: UITextView!
    @IBOutlet var submitButton: UIButton!
    
    let realm = try! Realm()
    var student: Student?
    let onlineManager = OnlineDatabaseManager()
    
    override func viewDidLoad() {
        noteTextView.delegate = self
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //clear previous text
        noteTextView.text = ""
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        guard !self.noteTextView.text.isEmpty else {
            showWarning()
            return
        }
        
        onlineManager.addNote(note: self.noteTextView.text,
                              studentId: self.student?.id ?? "")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func showWarning() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showAlert(title: "Can't be an empty note.", message: "Please add text to the note to add.")
    }
    
}
