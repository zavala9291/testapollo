//
//  ListOfNotesVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 5/1/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ListOfNotesVC: UITableViewController {
    
    let realm = try! Realm()
    var student: Student?
    let manager = OnlineDatabaseManager()
    var studentNotes = [StudentNote]()
    
    override func viewDidLoad() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(R.nib.noteCell)
        
        guard let fetchedStudent = self.student else {
            return
        }
        
        manager.syncNotesForStudent(studentId: fetchedStudent.id ?? "") { (completed) in
            if completed {
                self.reloadNotes()
            }
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addNote))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        reloadNotes()
    }
    
    func reloadNotes() {
        guard let fetchedStudent = self.student else {
            return
        }
        
        let predicate = NSPredicate(format: "studentId == %@", fetchedStudent.id ?? "")
        self.studentNotes = self.realm.objects(StudentNote.self).filter(predicate).toArray(ofType: StudentNote.self)
        self.tableView.reloadData()
    }
    
    func addNote() {
        let vc = R.storyboard.main.addNoteVC()
        guard let viewController = vc else {
            return
        }
        viewController.student = self.student
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // MARK - UITableview Methods
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.noteCell.identifier, for: indexPath) as? NoteCell else {
            log.error("failed to initialize home cell")
            return UITableViewCell()
        }
        
        let studentNote = self.studentNotes[indexPath.row]
        cell.configureCell(date: studentNote.date ?? "", note: studentNote.note ?? "")
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.studentNotes.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //selected cell
    }
}
