//
//  StudentsVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 9/17/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift

class StudentsVC: UITableViewController {
    
    var ref: DatabaseReference!
    var currentSchoolId = String()
    var currentSchoolName = String()
    var listOfStudents: Results<Student>?
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.lightGray
        self.tableView.register(R.nib.studentCheckCell)
        self.tableView.separatorStyle = .singleLine
        
        let addStudentButton = UIBarButtonItem(image: UIImage(named: "add"), style: .plain, target: self, action: #selector(StudentsVC.addStudentAction))
        navigationItem.rightBarButtonItem = addStudentButton
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(SchoolsVC.refresh(sender:)), for: UIControlEvents.valueChanged)
        
        //get selected school
        let settingsResults = realm.objects(SelectedSettings.self)
        
        guard let currentSettings = settingsResults.first else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showAlert(title: "School not yet selected", message: "Please Select a school to see list of students.")
            return
        }
        
        currentSchoolId = currentSettings.selectedSchoolId ?? ""
        self.fetchLocalStudents()

        let predicate = NSPredicate(format: "id == %@", currentSchoolId)
        guard let selectedSchool = self.realm.objects(School.self).filter(predicate).first else {
            log.error("unable to get current school")
            return
        }
        
        self.title = selectedSchool.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getData()
    }
    
    func addStudentAction() {
        let viewController = R.storyboard.main.addStudentVC()
        guard let vc = viewController else {
            log.error("unable to initialized view controller")
            return
        }
        vc.currentSchoolId = currentSchoolId
        navigationController?.pushViewController(vc, animated: true)
    }

    // MARK - UITableview Methods
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.studentCheckCell.identifier, for: indexPath) as? StudentCheckCell else {
            log.error("failed to initialize student check cell")
            return UITableViewCell()
        }
        
        guard let student = listOfStudents?[indexPath.row] else {
            //unable to get student
            return UITableViewCell()
        }
        
        cell.configureCell(name: student.name ?? "", studentPresent: true, row: indexPath.row, showToggle: false)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfStudents?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let student = listOfStudents?[indexPath.row] else {
            //unable to get student
            return
        }
        
        let viewController = R.storyboard.main.studentDetailsVC()
        guard let vc = viewController else {
            log.error("unable to initialized view controller")
            return
        }
        
        vc.student = student
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.getData()
    }
    
    func fetchLocalStudents() {
        let predicate = NSPredicate(format: "schoolId == %@", self.currentSchoolId)
        let savedStudents = self.realm.objects(Student.self).filter(predicate)
        
        self.listOfStudents = savedStudents
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    func getData() {
        fetchLocalStudents()
        tableView.reloadData()
        let manager = OnlineDatabaseManager()
        
        weak var weakSelf = self
        manager.syncLocalStudentList { (completed) in
            
            if !completed {
                log.debug("sync local student not completed")
                //show alert
                
            } else {
                log.debug("sync local student completed")
                weakSelf?.fetchLocalStudents()
                weakSelf?.tableView.reloadData()
            }
        }
    }
}
