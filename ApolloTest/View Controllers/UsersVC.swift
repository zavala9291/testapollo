//
//  UsersVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/8/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import ActionSheetPicker_3_0

class UsersVC: UITableViewController, userCellDelegate {
    
    var ref: DatabaseReference!
    let realm = try! Realm()
    var currentSelectedSchoolId: String?
    var manager = OnlineDatabaseManager()
    var allUsers: [AllUsers]?
    var cellDelegate: userCellDelegate?
    var allSchools: [School]?
    var allSchoolNames: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        self.tableView.backgroundColor = UIColor.lightGray
        self.tableView.separatorStyle = .singleLine
        self.title = "List of Users"
        self.allUsers = []
        self.allSchools = []
        self.allSchoolNames = []
        
        syncAllSchools()
        getUsers()
        
        self.tableView.backgroundColor = UIColor.lightGray
        self.tableView.register(R.nib.userCell)
        self.tableView.separatorStyle = .singleLine
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(UsersVC.refresh(sender:)), for: UIControlEvents.valueChanged)
        
        //get selected school
        let settingsResults = realm.objects(SelectedSettings.self)
        if let currentSettings = settingsResults.first {
            currentSelectedSchoolId = currentSettings.selectedSchoolId
        }
    }
    
    //sync schools
    func syncAllSchools() {
        weak var weakSelf = self
        manager.syncSchoolList { (completed) in
            weakSelf?.getSchoolNames()
        }
    }
    
    func getSchoolNames() {
        self.allSchoolNames?.removeAll()
        
        let allSchools = self.realm.objects(School.self)
        for school in allSchools {
            allSchoolNames?.append(school.name ?? "")
        }
    }
    
    //sync users
    func getUsers() {
        weak var weakSelf = self
        manager.getAllUsers { (completed) in
            if completed {
                
                weakSelf?.allUsers?.removeAll()
                
                guard let users = weakSelf?.realm.objects(AllUsers.self) else {
                    //unable to get users
                    return
                }
                for user in users {
                    weakSelf?.allUsers?.append(user)
                }
                weakSelf?.tableView.reloadData()
            }
        }
    }
    
    func reloadDatabaseData() {
        self.getUsers()
    }
    
    func refresh(sender:AnyObject) {
        self.reloadDatabaseData()
    }
    
    func changeUserApproval(approved: Bool, user: AllUsers) {
        let alert = UIAlertController.init(title: "Changing user permissions.", message: "Are you sure you want to remove or give access to the organization to this user?", preferredStyle: .alert)
        let alertAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            //go back to previous status
            self.tableView.reloadData()
        }
        
        let changePermissionAction = UIAlertAction.init(title: "Change Permission", style: .default) { (action) in
            self.changePermission(approved: approved, user: user)
        }
        alert.addAction(alertAction)
        alert.addAction(changePermissionAction)
        self.present(alert, animated: false, completion: nil)
    }
    
    func changePermission(approved: Bool, user: AllUsers) {
        //updating online database
        manager.updateUserApproval(user: user, approved: approved)
    }
    
    // MARK - UITableview Methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //maybe go into their account detail to make sure they are approving the correct user?
        self.tableView.deselectRow(at: indexPath, animated: true)

        guard let allNames = self.allSchoolNames else {
            return
        }
        
        var rolesArray = ["editor", "admin"]
        
        //initially point to correct value
        guard let currentUser = allUsers?[indexPath.row] else {
            return
        }
        
        if let indexOfCurrentRole = rolesArray.index(of: currentUser.role ?? "") {
            rolesArray.remove(at: indexOfCurrentRole)
            rolesArray.insert(currentUser.role ?? "", at: 0)
        }
        
        var allSchoolNames = allNames
        
        if let indexOfCurrentRole = allSchoolNames.index(of: currentUser.schoolName ?? "") {
            allSchoolNames.remove(at: indexOfCurrentRole)
            allSchoolNames.insert(currentUser.schoolName ?? "", at: 0)
        }
        
        weak var weakSelf = self
        ActionSheetMultipleStringPicker.show(withTitle: "Select new user values",
                                             rows: [rolesArray,allSchoolNames],
                                             initialSelection: [0,0],
                                             doneBlock: { (picker, values, indexes) in
                                                
                                                if let valuesPicked = indexes as? [String] {
                                                    let rolePicked = valuesPicked[0]
                                                    let schoolPicked = valuesPicked[1]
                                                    
                                                    if rolePicked != currentUser.role ?? "" {
                                                        //change to new role
                                                        weakSelf?.manager.updateUserRole(user: currentUser, role: rolePicked)
                                                    }
                                                    
                                                    if schoolPicked != currentUser.schoolName ?? "" {
                                                        //change to new school name
                                                        //change to new school id
                                                        weakSelf?.manager.updateUserSchool(user: currentUser, schoolName: schoolPicked)
                                                    }
                                                    
                                                }
                                                
                                                
                                                return
        },
                                             cancel: { (picker) in
                                                //cancel
        },
                                             origin: self.view)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.userCell.identifier, for: indexPath) as? UserCell else {
            log.error("failed to initialize home cell")
            return UITableViewCell()
        }
        
        guard let users = allUsers else {
            return UITableViewCell()
        }

        let user = users[indexPath.row]
        cell.configure(user: user)
        cell.delegate = self
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = allUsers?.count else {
            return 0
        }
        return count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

