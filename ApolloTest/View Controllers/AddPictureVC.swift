//
//  AddPictureVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 7/23/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage
import Progress

class AddPictureVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var photoLibraryButton: UIButton!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var uploadButton: UIButton!
    @IBOutlet var imageToUploadView: UIImageView!
    
    var imagePickerController = UIImagePickerController()
    var student: Student?
    var onlineManager = OnlineDatabaseManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerController.delegate = self
    }
    
    @IBAction func addFromLibraryAction(_ sender: Any) {
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func openCamera(_ sender: Any) {
        imagePickerController.sourceType = .camera
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard info[UIImagePickerControllerMediaType] != nil else { return }
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType  == "public.image" {
                log.debug("Image Selected")
            }
            
            if mediaType == "public.movie" {
                log.debug("Video Selected")
            }
        }
        
        
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageToUploadView.image = selectedImage
        } else {
            //didn't select an image, need to handle videos
            showImageAlert()
        }

        imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func uploadAction(_ sender: Any) {
        //upload image from image view
        
        guard let selectedImage = imageToUploadView.image else {
            showImageAlert()
            return
        }
        
        Prog.start(in: imageToUploadView, .blur(.dark), .activityIndicator)

        weak var weakSelf = self
        guard let currentStudent = self.student else {
            log.error("unable to get current student")
            return
        }
        
        onlineManager.setStudentPicture(idOfStudent: currentStudent.id, image: selectedImage) { (completed, task) in
            
            guard let uploadTask = task else {
                log.error("unable to get upload task")
                return
            }
            
            let observer = uploadTask.observe(.progress) { snapshot in
                // A progress event occured
                
                let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
                    / Double(snapshot.progress!.totalUnitCount)
                
                log.debug("the percent completed is: \(percentComplete)")
                
                if let view = weakSelf?.imageToUploadView {
                    Prog.update(Float(percentComplete), in: view)
                }
            }
            
            uploadTask.observe(.success) { snapshot in
                // Upload completed successfully
                
                if let view = weakSelf?.imageToUploadView {
                    Prog.end(in: view)
                }
                
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            uploadTask.observe(.failure) { snapshot in
                if let view = weakSelf?.imageToUploadView {
                    Prog.end(in: view)
                }
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showAlert(title: "Upload Failed", message: "Please check internet connection or try again later.")
            }
            
            uploadTask.resume()
        }
    }
    
    func showImageAlert() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showAlert(title: "Please select a photo", message: "Please select an image from your library or take one from the camera.")
    }
}
