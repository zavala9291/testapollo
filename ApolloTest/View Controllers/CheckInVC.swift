//
//  CheckInVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 11/1/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Firebase

class CheckInVC: UITableViewController {
    
    var ref: DatabaseReference!
    var listOfStudents: Results<Student>?
    var listOfSchoolDays: Results<SchoolDay>?
    var currentDate = String()
    
    //onlineDB
    var listOfOnlineSchoolDays = [OnlineSchoolDay]()
    
    var listOfStudentIds = [String]()
    var currentSchoolId = String()
    let realm = try! Realm()
    
    //completion tracker
    var completedStudentSync = false
    var completedCheckInSync = false
    
    let manager = OnlineDatabaseManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        self.tableView.backgroundColor = UIColor.lightGray
        self.tableView.register(R.nib.studentCheckCell)
        self.tableView.separatorStyle = .singleLine
        
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        currentDate = dateFormatter.string(from: now)
        self.title = currentDate
        
        let saveCheckInList = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(CheckInVC.submitAllStudentCheckIns))
        navigationItem.rightBarButtonItem = saveCheckInList
        
        let settingsResults = realm.objects(SelectedSettings.self)
        if let currentSettings = settingsResults.first {
            currentSchoolId = currentSettings.selectedSchoolId ?? ""
        }
        
        self.fetchLocalData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.reloadOnlineDatabase()
    }
    
    func submitAllStudentCheckIns() {

        guard let allStudents = listOfStudents else {
            return
        }
        
        let schoolDayId = "\(currentDate)-\(currentSchoolId)"
        
        for i in (0 ..< allStudents.count) {
            let student = allStudents[i]
            let cell = tableView.cellForRow(at: IndexPath.init(row: i, section: 0)) as? StudentCheckCell
            guard let isStudentPresent = cell?.studentPresentSwitch.isOn else {
                return
            }
            
            let newSchoolDay = SchoolDay.init(id: schoolDayId, schoolId: currentSchoolId, studentId: student.id, studentName: student.name, date: currentDate, present: isStudentPresent)
            manager.checkInStudent(schoolday: newSchoolDay)
        }
        
        showCompletion()
    }
    
    func showCompletion() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showAlert(title: "Checked in students.", message: "Attendance saved.")
    }
    
    func fetchLocalData() {
        let predicate = NSPredicate(format: "schoolId == %@", self.currentSchoolId)
        self.listOfStudents = self.realm.objects(Student.self).filter(predicate)
        
        let schoolPredicate = NSPredicate(format: "schoolId == %@ AND date == %@", self.currentSchoolId, self.currentDate)
        self.listOfSchoolDays = self.realm.objects(SchoolDay.self).filter(schoolPredicate)
        
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
        
        for studentId in listOfStudentIds {
            var isStudentPresent = false
            if let result = self.listOfSchoolDays?.filter({ (day) -> Bool in
                return day.studentId == studentId
            }).first {
                isStudentPresent = result.present
            }
            
            //default value is set to false, if its changed, it will be replace
            let day = OnlineSchoolDay(id: studentId , isPresent: isStudentPresent)
            self.listOfOnlineSchoolDays.append(day)
        }
    }
    
    func reloadOnlineDatabase() {
        manager.syncLocalStudentList { (completed) in
            
            if completed {
                DispatchQueue.main.async {
                    self.completedStudentSync = true
                    self.checkIfSyncsCompleted()
                }
            } else {
                guard let _ = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showAlert(title: "School not yet selected", message: "Please Select a school to check in students.")
                    return
                }
            }
        }
        
        let now = Date()
        manager.syncLocalAttendanceHistory(date: now) { (completed) in
            if completed {
                DispatchQueue.main.async {
                    self.completedCheckInSync = true
                    self.checkIfSyncsCompleted()
                }
            } else {
                log.error("did not completed attendance")
            }
        }
    }
    
    //need to reload tableview when both syncs completed
    func checkIfSyncsCompleted() {
        if self.completedStudentSync && self.completedCheckInSync {
            self.completedCheckInSync = false
            self.completedStudentSync = false
            self.fetchLocalData()
        }
    }
    
    //MARK: Tableview delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.studentCheckCell.identifier, for: indexPath) as? StudentCheckCell else {
            log.error("failed to initialize student check cell")
            return UITableViewCell()
        }
        
        guard let student = listOfStudents?[indexPath.row] else {
            //unable to get student
            return UITableViewCell()
        }
        
        var isStudentPresent = false
        if let result = self.listOfSchoolDays?.filter({ (day) -> Bool in
            return day.studentId == student.id
        }).first {
            isStudentPresent = result.present
        }
        
        cell.configureCell(name: student.name ?? "", studentPresent: isStudentPresent, row: indexPath.row, showToggle: true)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //selecting cell
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfStudents?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
