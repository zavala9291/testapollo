//
//  HomeCoordinator.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/25/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

protocol HomeCoordinatorDelegate: class {
    func homeCoordinatorChange(_ homeCoordinator: HomeCoordinator, didClose: CoordinatorCloseType)
}

class HomeCoordinator: CSCoordinator {
    
    weak var delegate: HomeCoordinatorDelegate?
    var rootViewController: UIViewController
    var displayedViewController: UIViewController?
    var childCoordinators: [CSCoordinator] = []
    
    /// The storyboard we create new view controllers from. Not using segues, only storyboard identifiers.
    @objc public var storyboard: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: Bundle.main)
    }
    
    // MARK: Localized Strings
    fileprivate static let bundle = Bundle.main
    
    required init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    func start() {
        guard let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC else {
            print("\(storyboard.description) storyboard unable to load HomeVC")
            delegate?.homeCoordinatorChange(self, didClose: .error)
            return
        }
        
        // Force instatiation of the view object to set the IBOutlets.  This calls viewDidLoad(:)
        _ = homeVC.view
        
        //TODO: setup delegate for VC
        homeVC.delegate = self
        
        DispatchQueue.main.async {
            self.present(view: homeVC, animated: true)
        }
        
        displayedViewController = homeVC
    }
    
    func stop() {
        for coordinator in childCoordinators {
            removeChildCoordinator(coordinator)
        }
        
        let strongSelf = self// else { return }
        DispatchQueue.main.async {
            guard let displayedVC = strongSelf.displayedViewController else {
                log.error("unable to get displayed VC")
                return
            }
            
            self.rootViewControllerDismiss(animated: true, intendedVCToPop: displayedVC)
            strongSelf.displayedViewController = nil
        }
    }
}

extension HomeCoordinator: HomeVCDelegate {
    func signedOut(_homeVC: HomeVC) {
        delegate?.homeCoordinatorChange(self, didClose: .completed)
    }
    
    func selectedRow(_homeVC: HomeVC, rowSelected: Int) {
        //TODO: add coordinators to other VC
        
        var viewController: UIViewController?
        
        switch rowSelected {
        case HomeVC.HomeSelections.listOfSchools.rawValue:
            viewController = R.storyboard.main.schoolsVC()
        case HomeVC.HomeSelections.listOfStudents.rawValue:
            viewController = R.storyboard.main.studentList()
        case HomeVC.HomeSelections.checkInStudents.rawValue:
            viewController = R.storyboard.main.checkInVC()
        case HomeVC.HomeSelections.attendanceHistory.rawValue:
            viewController = R.storyboard.main.attendanceHistoryVC()
        case HomeVC.HomeSelections.listOfUsers.rawValue:
            viewController = R.storyboard.main.usersVC()
        default:
            log.error("unable to navigate user")
            return
        }
        
        guard let vc = viewController else {
            log.error("unable to initialized view controller")
            return
        }
        
        _homeVC.navigationController?.pushViewController(vc, animated: true)
    }
}

