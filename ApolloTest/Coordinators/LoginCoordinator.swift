//
//  LoginCoordinator.swift
//
//  Created by Ismael Zavala on 9/26/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

/// Delegate corresponding to the LoginCoordinator
@objc public protocol LoginCoordinatorDelegate: class {
    
    /**
     Coordinator has completed its functionality, and is ready to be closed.
     
     The callee should call the `LoginCoordinator.stop()` function of the `LoginCoordinator` parameter
     in order to take control back from this coordinator.
     
     - parameters:
     - LoginCoordinator: Reference to the LoginCoordinator that fired the delegate
     - didClose: The reason the coordinator closed
     - withError: The error that triggerd the closing
     */
    func loginCoordinatorChange(_ loginCoordinator: LoginCoordinator, didClose: CoordinatorCloseType)
    
    @objc optional func navigationItemDisplayName(_ LoginCoordinator: LoginCoordinator) -> String
}

/// Coordinator responsible for allowing user to login
@objc public class LoginCoordinator: NSObject, CSCoordinator {
    
    /// The storyboard we create new view controllers from. Not using segues, only storyboard identifiers.
    @objc public var storyboard: UIStoryboard {
        return UIStoryboard(name: "Login", bundle: Bundle.main)
    }
    
    /// The root view controller for a coordinator.  Either UIViewController or UINavigationController
    public let rootViewController: UIViewController
    
    /// The current view controller that is being displayed
    public var displayedViewController: UIViewController?
    
    /// The array containing any child Coordinators
    public var childCoordinators: [CSCoordinator] = []
    
    /// LoginCoordinatorDelegate delegate
    @objc public weak var delegate: LoginCoordinatorDelegate?
    
    // MARK: Localized Strings
    fileprivate static let bundle = Bundle.main
    
    // MARK: Initializers
    required public init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    /// Tells the coordinator to take over the user flow.
    public func start() {
        
        guard let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {
            delegate?.loginCoordinatorChange(self, didClose: .error)
            return
        }
        
        loginVC.delegate = self
        
        _ = loginVC.view
        
        loginVC.navigationItem.title = delegate?.navigationItemDisplayName?(self) ?? Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
        
        DispatchQueue.main.async {
            self.present(view: loginVC, animated: true)
        }
        
        displayedViewController = loginVC
    }
    
    /// Tells the coordinator to give up control of user flow
    public func stop() {
        log.debug("stopping login coordinator")
        for coordinator in childCoordinators {
            removeChildCoordinator(coordinator)
        }
        
        DispatchQueue.main.async {
            if let displayedVC = self.displayedViewController {
                self.rootViewControllerDismiss(animated: true, intendedVCToPop: displayedVC)
                log.debug("poped vc from within login coor")
                self.displayedViewController = nil
            }
        }
    }
}

// MARK: Private Helpers
extension LoginCoordinator {
    //any possible
}

//MARK: LoginVC Delegate
extension LoginCoordinator: LoginVCDelegate {
    
    func registerButtonClicked(_loginVC: LoginVC) {
        
        let registerUserCoordinator = RegisterUserCoordinator.init(rootViewController: displayedViewController ?? rootViewController)
        registerUserCoordinator.delegate = self
        registerUserCoordinator.start()
        
        addChildCoordinator(registerUserCoordinator)
    }
    
    func login(_loginVC: LoginVC) {
        delegate?.loginCoordinatorChange(self, didClose: .completed)
    }
}

// MARK: RegisterUserCoordinatorDelegate
extension LoginCoordinator: RegisterUserCoordinatorDelegate {
    public func registerUserCoordinatorChange(_ registerUserCoordinator: RegisterUserCoordinator, didClose: CoordinatorCloseType) {
        removeChildCoordinator(registerUserCoordinator)
        
        if didClose == .userCancel {
            //user just navigated back, no need to do anything else
            //the navigation controller will automatically remove the VC due to user going back, removing it manually will just pop an extra VC
        } else if didClose == .completed {
            //login completed, no need for login coordinator
            log.error("register user did close by completed")
            delegate?.loginCoordinatorChange(self, didClose: .completed)
        }
    }
}

