//
//  AppCoordinator.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/24/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseStorage

class AppCoordinator: CSCoordinator {

    let rootViewController: UIViewController
    var displayedViewController: UIViewController?
    var childCoordinators = [CSCoordinator]()
    
    required init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    func start() {
        log.debug("starting app coordinator")
        //initial setup contains back button on intended root
        guard let root = rootController else {
            log.error("unable to get root VC")
            return
        }
        determineNextVC(root)
    }
    
    func stop() {
        //any additional steps to be done after coordinator stops
    }
    
    fileprivate func sendUserToOrgSetup(_ rootVC: RootViewController) {
        let orgSetupCoordinator = OrganizationSetupCoordinator(rootViewController: rootVC)
        orgSetupCoordinator.delegate = self
        orgSetupCoordinator.start()
        self.addChildCoordinator(orgSetupCoordinator)
    }
    
    fileprivate func determineNextVC(_ rootVC: RootViewController) {
        let existingCoordinators = childCoordinators.filter { $0 is LoginCoordinator || $0 is OrganizationSetupCoordinator } // change to also add home VC
        for existingCoordinator in existingCoordinators {
            removeChildCoordinator(existingCoordinator)
        }
        
        //not signed in
        if Auth.auth().currentUser == nil {
            log.debug("user is not signed in, starting at login VC")
            let homeCoordinator = LoginCoordinator(rootViewController: rootVC)
            homeCoordinator.delegate = self
            homeCoordinator.start()
            addChildCoordinator(homeCoordinator)
        } else {
            //determine if user has a group selected
            var onlineRef: DatabaseReference!
            onlineRef = Database.database().reference()
            guard let userId = Auth.auth().currentUser?.uid else {
                log.error("unable to get current user id")
                self.sendUserToOrgSetup(rootVC)
                return
            }
            
            log.debug("current user id is: \(userId)")
            weak var weakSelf = self
            onlineRef.child(UIDefinitions.allUsers).child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
                guard let fetchedDictionary = snapshot.value as? NSDictionary else {
                    log.error("unable to fetch all users dictionary")
                    weakSelf?.sendUserToOrgSetup(rootVC)
                    return
                }
                
                guard let databaseId = fetchedDictionary[UIDefinitions.databaseId] as? String, !databaseId.isEmpty else {
                    log.debug("User doesn't have database setup")
                    //send user to org setup
                    
                    DispatchQueue.main.async {
                        log.debug("user doesn't have database setup, starting at org setup")
                        let orgSetupCoordinator = OrganizationSetupCoordinator(rootViewController: rootVC)
                        orgSetupCoordinator.delegate = self
                        orgSetupCoordinator.start()
                        self.addChildCoordinator(orgSetupCoordinator)
                    }
                    return
                }
                
                log.debug("user is signed in and has org setup, starting at home vc")
                let homeCoordinator = HomeCoordinator(rootViewController: rootVC)
                homeCoordinator.delegate = self
                homeCoordinator.start()
                self.addChildCoordinator(homeCoordinator)
            })
        }
    }
}

// MARK: - Properties (computed)
extension AppCoordinator {
    var navController: UINavigationController? {
        return rootViewController as? UINavigationController
    }
    fileprivate var rootController: RootViewController? {
        return navController?.viewControllers.first as? RootViewController
    }
}

// MARK: - RootViewControllerDelegate
extension AppCoordinator: RootViewControllerDelegate {
    func presentHome(_ rootVC: RootViewController) {
        determineNextVC(rootVC)
    }
}

// MARK: - LoginCoordinatorDelegate
extension AppCoordinator: LoginCoordinatorDelegate {
    func loginCoordinatorChange(_ loginCoordinator: LoginCoordinator, didClose: CoordinatorCloseType) {
        // flow completed, so display root
        rootController?.delegate = self
        removeChildCoordinator(loginCoordinator)
    }
}

extension AppCoordinator: OrganizationSetupCoordinatorDelegate {
    func organizationCoordinatorChange(_ organizationCoordinator: OrganizationSetupCoordinator, didClose: CoordinatorCloseType) {
        // flow completed, so display root
        rootController?.delegate = self
        removeChildCoordinator(organizationCoordinator)
    }
}

extension AppCoordinator: HomeCoordinatorDelegate {
    func homeCoordinatorChange(_ homeCoordinator: HomeCoordinator, didClose: CoordinatorCloseType) {
        // flow completed, so display root
        rootController?.delegate = self
        removeChildCoordinator(homeCoordinator)
    }
}
