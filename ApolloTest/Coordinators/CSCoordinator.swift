//
//  CSCoordinator.swift
//
import Foundation
import UIKit

/// Coordinator closing reasons Enum
@objc public enum CoordinatorCloseType: Int {
    case save           = 0
    case delete         = 1
    case userCancel     = 2
    case error          = 3
    case completed      = 4
}

/// The Coordinator protocol
@objc public protocol CSCoordinator: class {

    /// The root view controller for a coordinator.  Either UIViewController or UINavigationController
    var rootViewController: UIViewController { get }

    /// The current view controller that is being displayed
    var displayedViewController: UIViewController? { get set }

    /// The array containing any child Coordinators
    var childCoordinators: [CSCoordinator] { get set }

    // Present modally from rootViewController
    init(rootViewController: UIViewController)

    /// Tells the coordinator to create its initial view controller and take over the user flow.
    func start()

    /// Tell the coordinator to dismiss any displayed VC (if applicable) and clean up any of its child coordinators
    func stop()
}

// Helper methods
extension CSCoordinator {
    public func present(view: UIViewController, animated: Bool) {
        // Ensure we're presenting a UIViewController
        if let rootNavigationController = self.rootViewController as? UINavigationController ?? self.rootViewController.navigationController {
            rootNavigationController.pushViewController(view, animated: animated)
            return
        }

        let rootNav = UINavigationController(rootViewController: view)
        rootNav.modalPresentationStyle = .fullScreen
        self.rootViewController.present(rootNav, animated: animated, completion: nil)
    }

    public func rootViewControllerDismiss(animated: Bool, intendedVCToPop: UIViewController) {
        if let rootNavigationController = self.rootViewController as? UINavigationController ?? self.rootViewController.navigationController {
            log.error("trying to remove the VC \(rootNavigationController.childViewControllers.last)")
            
            guard let lastVC = rootNavigationController.childViewControllers.last else {
                log.error("unable to get the last VC in the nav stack")
                return
            }
            
            guard lastVC == intendedVCToPop else {
                log.error("intended vc to pop doesn't match last VC on stack")
                return
            }
            
            rootNavigationController.popViewController(animated: animated)
            return
        }
        self.rootViewController.dismiss(animated: animated, completion: nil)
    }

    /// Add a child coordinator to the parent
    public func addChildCoordinator(_ childCoordinator: CSCoordinator) {
        self.childCoordinators.append(childCoordinator)
    }

    /// Remove a child coordinator from the parent
    public func removeChildCoordinator(_ childCoordinator: CSCoordinator) {
        childCoordinator.stop()
        self.childCoordinators = self.childCoordinators.filter { $0 !== childCoordinator }
    }
}
