//
//  OrganizationSetupCoordinator.swift
//  FriendlyFire
//
//  Created by Ismael Zavala on 9/26/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

protocol OrganizationSetupCoordinatorDelegate: class {
    func organizationCoordinatorChange(_ organizationCoordinator: OrganizationSetupCoordinator, didClose: CoordinatorCloseType)
}

class OrganizationSetupCoordinator: CSCoordinator {
    
    weak var delegate: OrganizationSetupCoordinatorDelegate?
    var rootViewController: UIViewController
    var displayedViewController: UIViewController?
    var childCoordinators: [CSCoordinator] = []
    var firstName = ""
    var lastName = ""
    
    /// The storyboard we create new view controllers from. Not using segues, only storyboard identifiers.
    @objc public var storyboard: UIStoryboard {
        return UIStoryboard(name: "OrganizationSetup", bundle: Bundle.main)
    }
    
    // MARK: Localized Strings
    fileprivate static let bundle = Bundle.main
    
    required init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    func start() {
        guard let organizationVC = storyboard.instantiateViewController(withIdentifier: "OrganizationSetupVC") as? OrganizationSetupVC else {
            print("\(storyboard.description) storyboard unable to load OrganizationSetupVC")
            delegate?.organizationCoordinatorChange(self, didClose: .error)
            return
        }
        
        // Force instatiation of the view object to set the IBOutlets.  This calls viewDidLoad(:)
        _ = organizationVC.view
        
        organizationVC.delegate = self
        
        DispatchQueue.main.async {
            self.present(view: organizationVC, animated: true)
        }
        
        displayedViewController = organizationVC
    }
    
    func start(first: String, last: String) {
        self.firstName = first
        self.lastName = last
    }
    
    func stop() {
        for coordinator in childCoordinators {
            removeChildCoordinator(coordinator)
        }
        
        let strongSelf = self// else { return }
        guard let displayedVC = strongSelf.displayedViewController else {
            log.error("unable to get current VC")
            return
        }
        
        DispatchQueue.main.async {
            self.rootViewControllerDismiss(animated: true, intendedVCToPop: displayedVC)
            self.displayedViewController = nil
        }
    }
}

extension OrganizationSetupCoordinator: OrganizationSetupVCDelegate {
    func joinButtonClicked(_orgVC: OrganizationSetupVC) {
        delegate?.organizationCoordinatorChange(self, didClose: .completed)
    }
    
    func createButtonClicked(_orgVC: OrganizationSetupVC) {
        delegate?.organizationCoordinatorChange(self, didClose: .completed)
    }
}

