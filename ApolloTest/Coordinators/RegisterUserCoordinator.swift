//
//  RegisterUserCoordinator.swift
//  FriendlyFire
//
//  Created by Ismael Zavala on 9/26/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

/// Delegate corresponding to the RegisterUserCoordinatorDelegate
@objc public protocol RegisterUserCoordinatorDelegate: class {
    
    /**
     Coordinator has completed its functionality, and is ready to be closed.
     
     The callee should call the `RegisterUserCoordinator.stop()` function of the `RegisterUserCoordinator` parameter
     in order to take control back from this coordinator.
     
     - parameters:
     - RegisterUserCoordinator: Reference to the RegisterUserCoordinator that fired the delegate
     - didClose: The reason the coordinator closed
     */
    func registerUserCoordinatorChange(_ registerUserCoordinator: RegisterUserCoordinator, didClose: CoordinatorCloseType)
    
    @objc optional func navigationItemDisplayName(_ RegisterUserCoordinator: RegisterUserCoordinator) -> String
}

/// Coordinator responsible for performing User Registration
@objc public class RegisterUserCoordinator: NSObject, CSCoordinator {
    
    /// The storyboard we create new view controllers from. Not using segues, only storyboard identifiers.
    @objc public var storyboard: UIStoryboard {
        return UIStoryboard(name: "RegisterUser", bundle: Bundle.main)
    }
    
    /// The root view controller for a coordinator.  Either UIViewController or UINavigationController
    public let rootViewController: UIViewController
    
    /// The current view controller that is being displayed
    public var displayedViewController: UIViewController?
    
    /// The array containing any child Coordinators
    public var childCoordinators: [CSCoordinator] = []
    
    /// RegisterUserCoordinatorDelegate delegate
    @objc public weak var delegate: RegisterUserCoordinatorDelegate?
    
    // MARK: Localized Strings
    fileprivate static let bundle = Bundle.main
    let controllerHeader = NSLocalizedString("Register User", bundle: bundle, comment: "Register User")
    
    
    // MARK: Initializers
    @objc required public init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    /// Tells the coordinator to take over the user flow.
    public func start() {
        
        guard let registerUserVC = storyboard.instantiateViewController(withIdentifier: "RegisterUserVC") as? RegisterUserVC else {
            print("\(storyboard.description) storyboard unable to load RegisterUserVC")
            delegate?.registerUserCoordinatorChange(self, didClose: .error)
            return
        }
        
        setup(registerUserViewController: registerUserVC)
        
        DispatchQueue.main.async {
            self.present(view: registerUserVC, animated: true)
        }
        
        displayedViewController = registerUserVC
    }
    
    /// Tells the coordinator to give up control of user flow
    public func stop() {
        for coordinator in childCoordinators {
            removeChildCoordinator(coordinator)
        }
        
        DispatchQueue.main.async {
            if let displayed = self.displayedViewController {
                //todo: this isn't included in org setup coor
                self.rootViewControllerDismiss(animated: true, intendedVCToPop: displayed)
                self.displayedViewController = nil
            }
        }
    }
    
    fileprivate func setup(registerUserViewController view: RegisterUserVC) {
        // Force instatiation of the view object to set the IBOutlets.  This calls viewDidLoad(:)
        _ = view.view
        
        //TODO: setup delegate for VC
        view.delegate = self
        
        view.navigationItem.title = controllerHeader
    }
}

// MARK: possibly login coordinator
extension RegisterUserCoordinator: RegisterUserVCDelegate {
    func didTapBackButton(_ withVC: RegisterUserVC) {
        //lets delegate know that user navigated back and there's no need for coordinator anymore
        delegate?.registerUserCoordinatorChange(self, didClose: .userCancel)
    }
    
    func createAccountButtonClicked(_loginVC: RegisterUserVC) {
        //dismiss and let app coordinator determine where to go afterwards
        delegate?.registerUserCoordinatorChange(self, didClose: .completed)
        
    }
}
