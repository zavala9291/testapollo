//
//  RootViewController.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/24/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

protocol RootViewControllerDelegate: class {
    func presentHome(_ rootVC: RootViewController)
}
class RootViewController: UIViewController {
    
    weak var delegate: RootViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.red
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        delegate?.presentHome(self)
    }
}
