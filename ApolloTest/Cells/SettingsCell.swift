//
//  SettingsCell.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 1/15/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

class SettingsCell: UITableViewCell {
    
    @IBOutlet var cellTitleLabel: UILabel!
    @IBOutlet var cellvalueLabel: UILabel!
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configureCell(title: String, value: String) {
        self.cellTitleLabel.text = title
        self.cellvalueLabel.text = value
        
        self.cellvalueLabel.textColor = UIColor.black
        self.cellTitleLabel.textColor = UIColor.black
        
        self.backgroundColor = UIColor.white
    }
}
