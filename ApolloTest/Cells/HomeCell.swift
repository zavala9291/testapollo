//
//  HomeCell.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 9/16/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

class HomeCell: UITableViewCell {
    
    @IBOutlet var cellTitleLabel: UILabel!
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configureCell(title: String) {
        self.cellTitleLabel.text = title
        self.cellTitleLabel.textColor = UIColor.black
        self.backgroundColor = UIColor.white

    }
}
