//
//  PhotoCell.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 8/3/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

class PhotoCell: UICollectionViewCell {

    @IBOutlet var photoView: UIImageView!
    @IBOutlet var checkmark: UIImageView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configureCell(image: UIImage, selected: Bool) {
        photoView.image = image
        checkmark.isHidden = !selected
    }
    
    public func configure(photo: Photo, selected: Bool) {
        checkmark.isHidden = !selected
        
        guard let thumbnailId = photo.thumbnailId else {
            log.error("unable to get thumbnail id")
            return
        }
        
        let locationOfPhoto = getDocumentsDirectory().appendingPathComponent("\(thumbnailId).jpg")
        let image = UIImage(contentsOfFile: locationOfPhoto.path)
        photoView.image = image
    }
    
    fileprivate func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
