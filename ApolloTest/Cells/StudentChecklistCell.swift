//
//  StudentChecklistCell.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 9/17/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

class StudentCheckCell: UITableViewCell {
    
    @IBOutlet var studentNameLabel: UILabel!
    @IBOutlet var studentPresentSwitch: UISwitch!
    var currentRow = 0
    
    @IBAction func presentToggleAction(_ sender: Any) {
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configureCell(name:String, studentPresent:Bool, row:Int, showToggle: Bool) {
        self.studentPresentSwitch.isHidden = !showToggle
        self.currentRow = row
        self.studentNameLabel.text = name
        self.studentNameLabel.textColor = UIColor.black
        self.backgroundColor = UIColor.white
        self.studentPresentSwitch.setOn(studentPresent, animated: false)
    }
}
