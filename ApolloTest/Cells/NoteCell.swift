//
//  NoteCell.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 5/15/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit

class NoteCell: UITableViewCell {
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var noteTextView: UITextView!
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configureCell(date: String, note: String) {
        self.dateLabel.text = date
        self.noteTextView.text = note
        self.noteTextView.sizeToFit()
    }
}
