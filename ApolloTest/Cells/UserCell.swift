//
//  UserCell.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/10/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

protocol userCellDelegate {
    func changeUserApproval(approved: Bool, user: AllUsers)
}

class UserCell: UITableViewCell {
    @IBOutlet var userLabel: UILabel!
    @IBOutlet var userSwitch: UISwitch!
    @IBOutlet var roleLabel: UILabel!
    @IBOutlet var schoolLabel: UILabel!
    var user: AllUsers?
    var delegate: userCellDelegate?
    
    func configure(user: AllUsers) {
        self.backgroundColor = .clear
        self.user = user
        self.userLabel.text = user.name ?? ""
        self.userSwitch.setOn(user.approved, animated: false)
        roleLabel.text = user.role ?? ""
        schoolLabel.text = user.schoolName ?? ""
    }
    
    @IBAction func userSwitchAction(_ sender: Any) {
        guard let currentUser = user else {
            return
        }
        
        delegate?.changeUserApproval(approved: userSwitch.isOn, user: currentUser)
    }
    
}
