//
//  SettingsVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 1/7/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import RealmSwift

class SettingsVC: UITableViewController {
    
    var ref: DatabaseReference!
    var firstname = ""
    var lastName = ""
    var adminApproved: Bool = false
    var emailVerified: Bool = false
    var selectedSchool: String?
    var userEmail = ""
    var role = ""
    var userGroup = ""
    var manager = OnlineDatabaseManager()
    fileprivate let realm = try! Realm()
    
    fileprivate enum SettingsSelections: Int {
        case firstName
        case lastName
        case adminApproved
        case emailVerified
        case selectedSchool
        case userEmail
        case role
        case userGroup
        case settingsSelectionsCount
        
        func localizedString() -> String {
            switch self {
            case .firstName:
                return NSLocalizedString("First name", comment: "First name")
            case .lastName:
                return NSLocalizedString("Last name", comment: "last name")
            case .adminApproved:
                return NSLocalizedString("Admin approved", comment: "admin approved")
            case .emailVerified:
                return NSLocalizedString("Email verified", comment: "Email verified")
            case .selectedSchool:
                return NSLocalizedString("Selected School", comment: "selected school")
            case .userEmail:
                return NSLocalizedString("Email", comment: "Email")
            case .role:
                return NSLocalizedString("Role", comment: "Role")
            case .userGroup:
                return NSLocalizedString("User Group", comment: "User Group")
            case .settingsSelectionsCount:
                return NSLocalizedString("error getting settings count", comment: "error")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        self.tableView.backgroundColor = UIColor.white
        self.tableView.register(R.nib.settingsCell)
        title = "Settings"
        
        getAccountData()
    }
    
    func getAccountData() {
        
        let user = Auth.auth().currentUser
        if let user = user {
            //getting account info
            userEmail = user.email ?? ""
            emailVerified = user.isEmailVerified
            
            //getting selected school info
            if let selectedSchoolId = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId {
                
                let allSchools = self.realm.objects(School.self)
                let filteredSchools = allSchools.filter { (school) -> Bool in
                    if school.id == selectedSchoolId {
                        return true
                    }
                    return false
                }
                selectedSchool = filteredSchools.first?.name ?? ""
            }
            
            //getting user info
            if let userInfo = self.realm.objects(User.self).first {
                self.getUserData(userInfo: userInfo)
            } else {
                weak var weakSelf = self
                manager.getDatabaseId(completion: { (id) in
                    if let userInfo = self.realm.objects(User.self).first {
                        weakSelf?.getUserData(userInfo: userInfo)
                    }
                })
            }
            self.tableView.reloadData()
        }
    }
    
    func getUserData(userInfo: User) {
        firstname = ""
        lastName = ""
        adminApproved = userInfo.approved

        if let fetchedGroupName = userInfo.databaseName, !fetchedGroupName.isEmpty {
            userGroup = fetchedGroupName
        } else {
            userGroup = "Unassigned"
        }
        
        if let fetchedRole = userInfo.role, !fetchedRole.isEmpty {
            role = fetchedRole
        } else {
            role = "Unassigned"
        }
        
        guard let fullName = userInfo.name else {
            log.error("unable to get name")
            return
        }
        
        firstname = fullName
        
        let separatedName = fullName.components(separatedBy: " ")
        guard let first = separatedName.first else {
            return
        }
        firstname = first
        
        guard let second = separatedName.last else {
            return
        }
        lastName = second
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.settingsCell.identifier, for: indexPath) as? SettingsCell else {
            log.error("failed to initialize home cell")
            return UITableViewCell()
        }
        
        guard let cellType = SettingsSelections(rawValue:indexPath.row) else {
            log.error("error getting cell type")
            return UITableViewCell()
        }
        
        var value = ""

        switch indexPath.row {
        case SettingsSelections.firstName.rawValue:
            value = firstname
        case SettingsSelections.lastName.rawValue:
            value = lastName
        case SettingsSelections.adminApproved.rawValue:
            value = adminApproved ? "Approved" : "Requires Approval"
        case SettingsSelections.emailVerified.rawValue:
            value = emailVerified ? "Verified" : "Needs Verification"
        case SettingsSelections.selectedSchool.rawValue:
            value = selectedSchool ?? "No School Selected"
        case SettingsSelections.userEmail.rawValue:
            value = userEmail
        case SettingsSelections.role.rawValue:
            value = self.role
        case SettingsSelections.userGroup.rawValue:
            value = self.userGroup
        default:
            value = ""
        }
        cell.configureCell(title: cellType.localizedString(), value: value)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SettingsSelections.settingsSelectionsCount.rawValue
    }
}
