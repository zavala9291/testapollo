//
//  OnlineDatabaseManager.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/31/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import Foundation
import Firebase
import RealmSwift
import FirebaseStorage

class OnlineDatabaseManager {
    fileprivate let realm = try! Realm()
    fileprivate var ref: DatabaseReference!
    
    var dbId = ""
    
    enum UserUpdate {
        case approved
        case databaseId
        case databaseName
        case name
        case role
        case schoolId
        case schoolName
    }
    
    init() {
        ref = Database.database().reference()
        
        self.getDatabaseId { (fetchedID) in
            if let id = fetchedID {
                self.dbId = id
            }
        }
    }
    
    //MARK: Database info sync
    func getDatabaseId(completion: @escaping (_ id: String?) -> Void) {
        
        if let userInfo = realm.objects(User.self).first {
            //there's already database info available
            completion(userInfo.databaseId)
            return
        }
        
        var onlineRef: DatabaseReference!
        onlineRef = Database.database().reference()
        let user = Auth.auth().currentUser
        
        guard let userId = user?.uid else {
            completion(nil)
            return
        }
        
        weak var weakSelf = self
        onlineRef.child(UIDefinitions.allUsers).child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let fetchedDictionary = snapshot.value as? NSDictionary else {
                completion(nil)
                return
            }
            
            guard let userDatabaseId = fetchedDictionary[UIDefinitions.databaseId] as? String,
                let userName = fetchedDictionary[UIDefinitions.name] as? String else {
                    completion(nil)
                    return
            }
            
            var userDatabaseName = ""
            if let fetchedUserDatabaseName = fetchedDictionary[UIDefinitions.databaseName] as? String {
                userDatabaseName = fetchedUserDatabaseName
            }
            
            var userRole = ""
            if let fetchedRole = fetchedDictionary[UIDefinitions.role] as? String {
                userRole = fetchedRole
            }
            
            var userApproved = false
            if let fetchedApproval = fetchedDictionary[UIDefinitions.approved] as? String {
                guard let calculatedApproval = Bool(fetchedApproval) else {
                    completion(nil)
                    return
                }
                
                userApproved = calculatedApproval
            } else {
                guard let calculatedApproval = fetchedDictionary[UIDefinitions.approved] as? Bool else {
                    completion(nil)
                    return
                }
                userApproved = calculatedApproval
            }
            
            
            
            try! weakSelf?.realm.write {
                if let oldUsers = weakSelf?.realm.objects(User.self) {
                    weakSelf?.realm.delete(oldUsers)
                }
                if let oldDbId = weakSelf?.realm.objects(DBInfo.self) {
                    weakSelf?.realm.delete(oldDbId)
                }
            }
            
            let dbInfo = DBInfo.init(currentDBName: userDatabaseName, currentDBId: userDatabaseId)
            
            let user = User.init(newApproved: userApproved, newDatabaseId: userDatabaseId, newDatabaseName: userDatabaseName, newUserId: userId, newRole: userRole, newName: userName)
            //delete previous instances of the user in database
            
            try! weakSelf?.realm.write {
                weakSelf?.realm.add(user)
                weakSelf?.realm.add(dbInfo)
            }
            
            completion(userDatabaseId)
        })
    }
    
    func getCurrentOnlineUser(completion: @escaping (_ completed: Bool, _ userApproved: Bool) -> Void) {
        var onlineRef: DatabaseReference!
        onlineRef = Database.database().reference()
        let user = Auth.auth().currentUser
        
        guard let userId = user?.uid else {
            completion(false, false)
            log.error("unable to get current user id")
            return
        }
        
        weak var weakSelf = self
        onlineRef.child(UIDefinitions.allUsers).child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let fetchedDictionary = snapshot.value as? NSDictionary else {
                completion(false, false)
                return
            }
            
            guard let userRole = fetchedDictionary[UIDefinitions.role] as? String,
                let userDatabaseId = fetchedDictionary[UIDefinitions.databaseId] as? String,
                let userName = fetchedDictionary[UIDefinitions.name] as? String else {
                    //                    let schoolID = userDict[UIDefinitions.schoolId] as? String, //todo: school id should be set by admin
                    //                let schoolName = userDict[UIDefinitions.schoolName] as? String else { //todo: name should also set by admin
                    completion(false, false)
                    return
            }
            
            //evaluating user approval
            var userApproved = false
            if let fetchedApproval = fetchedDictionary[UIDefinitions.approved] as? String {
                guard let calculatedApproval = Bool(fetchedApproval) else {
                    completion(false, false)
                    return
                }
                
                userApproved = calculatedApproval
            } else {
                guard let calculatedApproval = fetchedDictionary[UIDefinitions.approved] as? Bool else {
                    completion(false, false)
                    return
                }
                userApproved = calculatedApproval
            }
            
            //evaluate Database Name
            var databaseName = ""
            if let fetchedDatabaseName = fetchedDictionary[UIDefinitions.databaseName] as? String {
                databaseName = fetchedDatabaseName
            }
            
            //update DB Id
            weakSelf?.dbId = userDatabaseId
            
            if let oldUserInfo = self.realm.objects(User.self).first {
                if let oldDbInfo = weakSelf?.realm.objects(DBInfo.self).first {
                    try! weakSelf?.realm.write {
                        oldDbInfo.currentDBId = userDatabaseId
                    }
                }
                
                //update old user info
                try! weakSelf?.realm.write {
                    oldUserInfo.approved = userApproved
                    oldUserInfo.databaseId = userDatabaseId
                    oldUserInfo.databaseName = databaseName
                    oldUserInfo.name = userName
                    oldUserInfo.role = userRole
                }
            } else {
                let newUserInfo = User(newApproved: userApproved,
                                       newDatabaseId: userDatabaseId,
                                       newDatabaseName: databaseName,
                                       newUserId: userDatabaseId,
                                       newRole: userRole,
                                       newName: userName)
                
                //create new
                try! weakSelf?.realm.write {
                    weakSelf?.realm.add(newUserInfo)
                }
                
                if let oldDbInfo = weakSelf?.realm.objects(DBInfo.self).first {
                    try! weakSelf?.realm.write {
                        oldDbInfo.currentDBId = userDatabaseId
                    }
                } else {
                    let newDBInfo = DBInfo.init(currentDBName: "", currentDBId: userDatabaseId)
                    try! weakSelf?.realm.write {
                        weakSelf?.realm.add(newDBInfo)
                    }
                }
            }
            completion(true, userApproved)
        })
    }
    
    //MARK: All users sync
    func getAllUsers(completion: @escaping (_ completed: Bool) -> Void) {
        var onlineRef: DatabaseReference!
        onlineRef = Database.database().reference()
        let user = Auth.auth().currentUser
        
        guard let userId = user?.uid else {
            completion(false)
            return
        }

        guard let dbInfo = realm.objects(DBInfo.self).first else {
                return
        }
        
        let dbId = dbInfo.currentDBId
        
        //delete previous instances of the user in database
        try! self.realm.write {
            let oldUsers = self.realm.objects(AllUsers.self).toArray(ofType: AllUsers.self)
            if oldUsers.count > 0 {
                self.realm.delete(oldUsers)
            }
        }
        
        weak var weakSelf = self
        onlineRef.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allUsers).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let fetchedDictionary = snapshot.value as? NSDictionary else {
                completion(false)
                return
            }
            
            for key in fetchedDictionary.allKeys {
                
                guard let id = key as? String else {
                    continue
                }
                
                if id == userId {
                    //dont want to include current user in this list
                    continue
                }
                
                guard let userDict = fetchedDictionary[key] as? NSDictionary else {
                    continue
                }
                
                guard let userRole = userDict[UIDefinitions.role] as? String,
                    let userApproved = userDict[UIDefinitions.approved] as? Bool,
                    let userDatabaseId = userDict[UIDefinitions.databaseId] as? String,
                    let userName = userDict[UIDefinitions.name] as? String else {
                    //                    let schoolID = userDict[UIDefinitions.schoolId] as? String, //todo: school id should be set by admin
                        //                let schoolName = userDict[UIDefinitions.schoolName] as? String else { //todo: name should also set by admin
                        continue
                }
                
                let newUser = AllUsers(newApproved: userApproved, newDatabaseId: userDatabaseId, newUserId: id, newRole: userRole, newName: userName, newSchoolId: "", newSchoolName: "")
                
                try! weakSelf?.realm.write {
                    weakSelf?.realm.add(newUser)
                }
            }
            completion(true)
        })
    }
    
    //MARK: add student
    func addStudent(name: String) {
        let uuid = UUID().uuidString
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            return
        }
        
        //add locally
        let studentToAdd = Student(name: name, id: uuid, contactNumber: nil, schoolId: schoolID)
        try! self.realm.write {
            self.realm.add(studentToAdd)
        }
        
        //add to online db
        self.ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allSchools).child(schoolID).child(uuid).setValue(["name":name])
        
    }
    
    //MARK: add student Note
    func addNote(note: String, studentId: String) {
        let uuid = UUID().uuidString
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            return
        }
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        let currentDate = dateFormatter.string(from: now)
        
        //add locally
        let noteToAdd = StudentNote(id: uuid, studentId: studentId, note: note, date: currentDate)
        try! self.realm.write {
            self.realm.add(noteToAdd)
        }
        
        let post = ["date": currentDate, "note": note] as [String : Any]

        //add to online db
        self.ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.AllNotes).child(schoolID)
            .child(studentId).child(uuid).setValue(post)
        
    }
    
    func syncNotesForStudent(studentId: String, completion: @escaping (_ completed: Bool) -> Void) {
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            return
        }
        
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.AllNotes)
            .child(schoolID).child(studentId).observeSingleEvent(of: .value, with: { (snapshot) in
                
                guard let value = snapshot.value as? NSDictionary else {

                    return
                }
                
                for id in value.allKeys {
                    guard let noteId = id as? String else {
                        continue
                    }
                    
                    guard let noteDict = value[noteId] as? NSDictionary else {
                        continue
                    }
                    
                    guard let note = noteDict[UIDefinitions.note] as? String else {
                        continue
                    }
                    
                    guard let date = noteDict[UIDefinitions.date] as? String else {
                        continue
                    }
                    
                    let studentNote = StudentNote.init(id: noteId, studentId: studentId, note: note, date: date)
                    
                    let predicate = NSPredicate(format: "id == %@", noteId)
                    if let _ = self.realm.objects(StudentNote.self).filter(predicate).first {
                        //No need to save, it already exists in DB
                        continue
                    }
                    
                    try! self.realm.write {
                        self.realm.add(studentNote)
                    }
                }
                
                completion(true)
        }) { (error) in
            log.error(error.localizedDescription)
            completion(false)
        }
    }
    
    //MARK: Check in student
    
    func checkInStudent(schoolday: SchoolDay) {
        
        //locally, need to remove old objects and just add the new ones
        let schoolDayPredicate = NSPredicate(format: "id == %@", schoolday.id ?? "")
        let allSchooldaysForCurrentSchool = self.realm.objects(SchoolDay.self).filter(schoolDayPredicate)
        
        try! realm.write {
            self.realm.delete(allSchooldaysForCurrentSchool)
        }
        
        guard let schoolDayId = schoolday.id else {
            return
        }
        
        guard let studentId = schoolday.studentId else {
            return
        }
        
        guard let schoolId = schoolday.schoolId else {
            return
        }
        
        let post = ["isPresent": schoolday.present, "name": schoolday.studentName ?? "", "date": schoolday.date
             ?? ""] as [String : Any]
        
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allCheckIns).child(schoolId).child(schoolDayId).child(studentId).setValue(post)
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.AllStudentsCheckIns).child(studentId).child(schoolDayId).setValue(post)
    }
    
    func getHistoryOfDate(id: String, completion: @escaping (_ completed: Bool) -> Void) {
        
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            return
        }
        
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allCheckIns).child(schoolID).child(id).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let checkInDictionary = snapshot.value as? NSDictionary else {
                log.error("couldn't get dictionary")
                completion(false)
                return
            }
            
            for fetchedStudentId in checkInDictionary.allKeys {
                guard let studentDictionary = checkInDictionary[fetchedStudentId] as? NSDictionary else {
                    return
                }
                
                guard let studentId = fetchedStudentId as? String else {
                    completion(false)
                    return
                }
                
                let studentPredicate = NSPredicate(format: "id == %@", studentId)
                guard let fetchedStudent = self.realm.objects(Student.self).filter(studentPredicate).first else {
                    return
                }
                
                guard let studentName = fetchedStudent.name else {
                    return
                }
                
                guard let date = studentDictionary["date"] as? String else {
                    return
                }
                
                guard let isPresent = studentDictionary["isPresent"] as? Bool else {
                    return
                }
                
                let schoolDay = SchoolDay.init(id: id, schoolId: schoolID, studentId: studentId, studentName: studentName, date: date, present: isPresent)
                
                let attendancePredicate = NSPredicate(format: "studentId == %@ AND id == %@", studentId, id)
                if self.realm.objects(SchoolDay.self).filter(attendancePredicate).toArray(ofType: SchoolDay.self).count > 0 {
                    //already exists in DB, skip
                } else {
                    try! self.realm.write {
                        self.realm.add(schoolDay)
                    }
                }
            }
            
            completion(true)
        }) { (error) in
            log.error(error.localizedDescription)
        }
    }

    //MARK: Sync School Days
    func syncOnlineAndLocalSchoolDays(dateId: String, completion: @escaping (_ finished: Bool) -> Void) {
        
        let settingsResults = realm.objects(SelectedSettings.self)
        guard let currentSchool = settingsResults.first?.selectedSchoolId else {
            completion(false)
            return
        }
        
        //schoolDay ID
        let onlineSchoolDayId = "\(dateId)-\(currentSchool)"
        let schoolDayPredicate = NSPredicate(format: "id == %@", onlineSchoolDayId)
        let allSchooldaysForCurrentSchool = self.realm.objects(SchoolDay.self).filter(schoolDayPredicate).toArray(ofType: SchoolDay.self) as [SchoolDay]
        
        if allSchooldaysForCurrentSchool.count > 0 {
            //remove old school days
            try! self.realm.write {
                realm.delete(allSchooldaysForCurrentSchool)
            }
        }
        
        ref = Database.database().reference()
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allCheckIns).child(currentSchool).child(onlineSchoolDayId).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? NSDictionary else {
                completion(false)
                return
            }
            
            for id in dictionary.allKeys {
                guard let fetchedId = id as? String else {
                    continue
                }
                
                guard let userDict = dictionary[fetchedId] as? NSDictionary else {
                    continue
                }
                
                guard let studentDate = userDict[UIDefinitions.date] as? String,
                    let studentName = userDict[UIDefinitions.name] as? String,
                    let isPresent = userDict[UIDefinitions.isPresent] as? Bool else {
                    continue
                }
                
                let fetchedSchoolDay = SchoolDay.init(id: onlineSchoolDayId,
                                                      schoolId: currentSchool,
                                                      studentId: fetchedId,
                                                      studentName: studentName,
                                                      date: studentDate,
                                                      present: isPresent)
                
                //add school day to DB
                try! self.realm.write {
                    self.realm.add(fetchedSchoolDay)
                }
            }
            completion(true)
            
        }) { (error) in
            log.error(error.localizedDescription)
        }
    }
    
    func syncLocalAttendanceHistory(date: Date, completion: @escaping (_ finished: Bool) -> Void) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        //id of school day will be "Nov 4, 2017"
        let dateString = dateFormatter.string(from: date)
        
        self.syncOnlineAndLocalSchoolDays(dateId: dateString, completion: completion)
    }
    
    //MARK: schools sync
    func syncSchoolList(completion: @escaping (_ finished: Bool) -> Void) {
        ref = Database.database().reference()
        
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allSchools).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let value = snapshot.value as? NSDictionary else {
                completion(false)
                return
            }
            
            for id in value.allKeys {
                guard let schoolId = id as? String else {
                    continue
                }
                
                guard let schoolDict = value[schoolId] as? NSDictionary else {
                    continue
                }
                
                guard let schoolName = schoolDict[UIDefinitions.schoolName] as? String else {
                    continue
                }
                
                let newSchool = School.init(name: schoolName, id: schoolId)
                let predicate = NSPredicate(format: "id == %@", schoolId)
                if let _ = self.realm.objects(School.self).filter(predicate).first {
                    //No need to save, it already exists in DB
                    continue
                }

                try! self.realm.write {
                    self.realm.add(newSchool)
                }
            }
            completion(true)
            return
            
        }) { (error) in
            log.error(error.localizedDescription)
            completion(false)
        }
    }
    
    //MARK: students sync
    func syncLocalStudentList(completion: @escaping (_ finished: Bool) -> Void) {
        
        ref = Database.database().reference()
        
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            completion(false)
            return
        }
        
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allSchools).child(schoolID).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else {
                completion(false)
                return
            }
            
            for id in value.allKeys {
                guard let studentId = id as? String else {
                    continue
                }
                
                guard let userDict = value[studentId] as? NSDictionary else {
                    continue
                }
                
                guard let studentName = userDict[UIDefinitions.name] as? String else {
                    continue
                }
                
                let fetchedStudent = Student.init(name: studentName, id: studentId, contactNumber: nil, schoolId: schoolID)
                
                let predicate = NSPredicate(format: "id == %@", studentId)
                if let _ = self.realm.objects(Student.self).filter(predicate).first {
                    //No need to save, it already exists in DB
                    continue
                }
                
                //save new student
                try! self.realm.write {
                    self.realm.add(fetchedStudent)
                }
            }
            completion(true)
            return
        }) { (error) in
            log.error(error.localizedDescription)
            completion(false)
        }
    }

    //MARK: User updates
    func updateUserSchool(user: AllUsers, schoolName: String) {
        //get school id from school name
        let schoolDayPredicate = NSPredicate(format: "name == %@", schoolName)
        let allSchoolForName = self.realm.objects(School.self).filter(schoolDayPredicate)
        
        guard let schoolToSwitch = allSchoolForName.first else {
            log.error("unable to get school based on name")
            return
        }
        
        //update the user in local DB
        try! realm.write {
            user.schoolName = schoolToSwitch.name
            user.schoolId = schoolToSwitch.id
        }
        
        //update the user in online DB
        updateUser(user: user, dataToUpdate: .schoolName, updatedString: schoolToSwitch.name, updatedBool: nil)
        updateUser(user: user, dataToUpdate: .schoolId, updatedString: schoolToSwitch.id, updatedBool: nil)
    }
    
    func updateUserApproval(user: AllUsers, approved: Bool) {
        //update the user in local DB
        try! realm.write {
            user.approved = approved
        }
        
        //update the user in online DB
        updateUser(user: user, dataToUpdate: .approved, updatedString: nil, updatedBool: approved)
    }
    
    func updateUserRole(user: AllUsers, role: String) {
        //update the user in local DB
        try! realm.write {
            user.role = role
        }
        
        //update the user in online DB
        updateUser(user: user, dataToUpdate: .role, updatedString: role, updatedBool: nil)
    }
    
    fileprivate func updateUser(user: AllUsers, dataToUpdate: UserUpdate, updatedString: String?, updatedBool: Bool?) {
        
        var nodeToUpdate = ""
        
        switch dataToUpdate {
        case .approved:
            nodeToUpdate = UIDefinitions.approved
        case .databaseId:
            nodeToUpdate = UIDefinitions.databaseId
        case .databaseName:
            nodeToUpdate = UIDefinitions.databaseName
        case .name:
            nodeToUpdate = UIDefinitions.name
        case .role:
            nodeToUpdate = UIDefinitions.role
        case .schoolId:
            nodeToUpdate = UIDefinitions.schoolId
        case .schoolName:
            nodeToUpdate = UIDefinitions.schoolName
        }
        
        guard let dbInfo = realm.objects(DBInfo.self).first else {
            return
        }
        let dbName = dbInfo.currentDBName
        
        guard let userId = user.userId,
            let dbId = user.databaseId else {
                return
        }
        
        if let newStringValue = updatedString {
            ref.child(UIDefinitions.allUsers).child(userId).child(nodeToUpdate).setValue(newStringValue)
            ref.child(UIDefinitions.allDatabaseInfo).child(dbName).child(UIDefinitions.users).child(userId).child(nodeToUpdate).setValue(newStringValue)
            ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allUsers).child(userId).child(nodeToUpdate).setValue(newStringValue)
            return
        }
        
        guard let newBoolValue = updatedBool else {
            return
        }
        
        ref.child(UIDefinitions.allUsers).child(userId).child(nodeToUpdate).setValue(newBoolValue)
        ref.child(UIDefinitions.allDatabaseInfo).child(dbName).child(UIDefinitions.users).child(userId).child(nodeToUpdate).setValue(newBoolValue)
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.allUsers).child(userId).child(nodeToUpdate).setValue(newBoolValue)
    }
    
    //MARK: student pictures
    func getStudentPictures(idOfStudent: String?, completion: @escaping (_ finished: Bool) -> Void) {
        ref = Database.database().reference()
        
        guard let studentId = idOfStudent else {
            log.error("unable to get student id")
            return
        }
        
        //check if school was selected
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            completion(false)
            return
        }
        
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.AllPictures).child(schoolID).child(studentId).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else {
                completion(false)
                return
            }
            
            for id in value.allKeys {
                guard let thumbnailId = id as? String else {
                    continue
                }
                
                guard let photoDict = value[thumbnailId] as? NSDictionary else {
                    continue
                }
                
                guard let mainPhotoId = photoDict[UIDefinitions.mainPhotoId] as? String else {
                    continue
                }
                
                let fetchedPhoto = Photo(thumbnailId: thumbnailId, id: mainPhotoId, studentId: studentId, schoolId: schoolID)

                let localURL = self.getDocumentsDirectory().appendingPathComponent("\(thumbnailId).jpg")
                let fileManager = FileManager.default
                let fileExists = fileManager.fileExists(atPath: localURL.path)
                
                let predicate = NSPredicate(format: "thumbnailId == %@", thumbnailId)
                if let _ = self.realm.objects(Photo.self).filter(predicate).first, fileExists {
                    //No need to save, it already exists in DB and file is already downloaded
                    continue
                }

                // Create a reference to the file you want to download
                let storageRef = Storage.storage().reference()
                let islandRef = storageRef.child("images/\(self.dbId)/\(schoolID)/\(studentId)/\(thumbnailId).JPG")
                
                // Download to the local filesystem
                let downloadTask = islandRef.write(toFile: localURL) { url, error in
                    if let error = error {
                        log.error(error.localizedDescription)
                        completion(false)
                    } else {
                        //save new student
                        try! self.realm.write {
                            self.realm.add(fetchedPhoto)
                        }
                        completion(true)
                    }
                }
            }
            completion(true)
        }) { (error) in
            log.error(error.localizedDescription)
            completion(false)
        }
    }
    
    func setStudentPicture(idOfStudent: String?, image: UIImage, completion: @escaping (_ finished: Bool, _ uploadTask: StorageUploadTask?) -> Void) {
        ref = Database.database().reference()
        
        guard let studentId = idOfStudent else {
            log.error("unable to get student id")
            return
        }
        
        //check if school was selected
        guard let schoolID = self.realm.objects(SelectedSettings.self).first?.selectedSchoolId else {
            log.error("unable to get school id")
            completion(false, nil)
            return
        }
        
        let newThumbnailId = UUID().uuidString
        let newMainId = UUID().uuidString
        
        //TODO: look into a way we could upload thumbnail easily, done, but then upload full res also, but with user not being stuck waiting for it to finish.

        //image data
        guard let data = UIImageJPEGRepresentation(image, 0.2) else { //having it set to 1 is full res and drags uploading it, maybe upload compressed one as thumb and then upload full res
            log.error("unable to get image data")
            return
        }
        
        //TODO: need to set limits on uploads, can't get too crazy
        log.debug("There were \(data.count) bytes")
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let string = bcf.string(fromByteCount: Int64(data.count))
        log.debug("formatted result: \(string)")
        
        // Create a reference to the file you want to upload
        let storageRef = Storage.storage().reference()
        let riversRef = storageRef.child("images/\(self.dbId)/\(schoolID)/\(studentId)/\(newThumbnailId).JPG")

        let uploadTask = riversRef.putData(data, metadata: nil) { (metadata, error) in
            log.debug("upload task, the error is: \(error) and metadata is: \(metadata)")
            
            guard let metadata = metadata else {
                log.error("unable to get metadata")
                completion(false, nil)
                return
            }
            
//            // Metadata contains file metadata such as size, content-type.
//            let size = metadata.size
//            // You can also access to download URL after upload.
//            riversRef.downloadURL { (url, error) in
//                print("the url is: \(url) and the error is: \(error)")
//                guard let downloadURL = url else {
//                    // Uh-oh, an error occurred!
//                    return
//                }
//            }
        }
        
        //setting the upload in the DB for reference
        ref.child(UIDefinitions.allDatabases).child(dbId).child(UIDefinitions.AllPictures).child(schoolID).child(studentId).child(newThumbnailId).setValue(["mainPhotoId":newMainId])
        
        completion(true, uploadTask)

    }
    
    fileprivate func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
}
