//
//  StudentNote.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 5/4/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import UIKit
import RealmSwift

class StudentNote: Object {
    public dynamic var studentId: String?
    public dynamic var note: String?
    public dynamic var id: String?
    public dynamic var date: String?
    
    convenience init(id: String?, studentId: String?, note: String?, date: String?) {
        self.init()
        self.id = id
        self.note = note
        self.studentId = studentId
        self.date = date
    }
}
