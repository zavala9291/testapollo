//
//  User.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/7/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    public dynamic var approved = false
    public dynamic var databaseId: String?
    public dynamic var databaseName: String?
    public dynamic var userId: String?
    public dynamic var role: String?
    public dynamic var name: String?
    
    convenience init(newApproved: Bool, newDatabaseId: String, newDatabaseName: String, newUserId: String, newRole: String, newName: String) {
        self.init()
        self.approved = newApproved
        self.databaseId = newDatabaseId
        self.databaseName = newDatabaseName
        self.userId = newUserId
        self.role = newRole
        self.name = newName
    }
}
