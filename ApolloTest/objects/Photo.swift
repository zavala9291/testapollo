//
//  Photo.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 8/3/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import UIKit
import RealmSwift

class Photo: Object {
    public dynamic var thumbnailId: String?
    public dynamic var id: String?
    public dynamic var studentId: String?
    public dynamic var schoolId: String?
    
    convenience init(thumbnailId: String?,
                     id: String?,
                     studentId: String?,
                     schoolId: String?) {
        self.init()
        self.thumbnailId = thumbnailId
        self.id = id
        self.studentId = studentId
        self.schoolId = schoolId
    }
}
