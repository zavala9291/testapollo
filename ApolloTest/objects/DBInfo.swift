//
//  DBInfo.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/2/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import RealmSwift

class DBInfo: Object {
    public dynamic var currentDBName = String()
    public dynamic var currentDBId = String()
    
    convenience init(currentDBName: String, currentDBId: String) {
        self.init()
        self.currentDBName = currentDBName
        self.currentDBId = currentDBId
    }
}
