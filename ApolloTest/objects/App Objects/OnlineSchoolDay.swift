//
//  OnlineSchoolDay.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 11/5/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import Foundation
class OnlineSchoolDay {
    var studentId = String()
    var isPresent = Bool()
    
    convenience init(id: String, isPresent: Bool) {
        self.init()
        self.studentId = id
        self.isPresent = isPresent
    }
}
