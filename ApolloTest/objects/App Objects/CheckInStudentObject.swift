//
//  CheckInStudentObject.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 1/28/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation

//model used to facilitate Check In VC
class CheckInStudentObject {
    var id: String
    var present: Bool
    var name: String
    
    init(name: String, id: String, present: Bool) {
        self.id = id
        self.present = present
        self.name = name
    }
    
}
