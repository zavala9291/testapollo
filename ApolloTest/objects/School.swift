//
//  School.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/16/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import UIKit
import RealmSwift

class School: Object {
    public dynamic var name: String?
    public dynamic var id: String?
    
    convenience init(name: String?,
         id: String?) {
        self.init()
        
        self.name = name
        self.id = id
    }
}
