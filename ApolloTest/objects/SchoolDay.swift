//
//  SchoolDay.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/16/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import UIKit
import RealmSwift

class SchoolDay: Object {
    public dynamic var id: String?
    public dynamic var schoolId: String?
    public dynamic var studentId: String?
    public dynamic var date: String?
    public dynamic var present = false
    public dynamic var studentName: String?
    
    convenience init(id: String?,
         schoolId: String?,
         studentId: String?,
         studentName: String?,
         date: String?,
         present: Bool) {
        
        self.init()
        self.id = id
        self.schoolId = schoolId
        self.studentId = studentId
        self.date = date
        self.present = present
        self.studentName = studentName
    }
}
