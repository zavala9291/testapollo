//
//  SelectedSettings.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/16/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import UIKit
import RealmSwift

class SelectedSettings: Object {
    public dynamic var selectedSchoolId: String?
    
    convenience init(selectedSchoolId: String?) {
        self.init()
        self.selectedSchoolId = selectedSchoolId
    }
}
