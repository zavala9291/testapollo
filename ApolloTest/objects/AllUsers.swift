//
//  AllUsers.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/8/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import RealmSwift

class AllUsers: Object {
    public dynamic var approved = false
    public dynamic var databaseId: String?
    public dynamic var userId: String?
    public dynamic var role: String?
    public dynamic var name: String?
    public dynamic var schoolId: String?
    public dynamic var schoolName: String?
    
    convenience init(newApproved: Bool, newDatabaseId: String, newUserId: String, newRole: String, newName: String, newSchoolId: String, newSchoolName: String) {
        self.init()
        self.approved = newApproved
        self.databaseId = newDatabaseId
        self.userId = newUserId
        self.role = newRole
        self.name = newName
        self.schoolId = newSchoolId
        self.schoolName = newSchoolName
    }
}
