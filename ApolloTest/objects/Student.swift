//
//  Student.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 10/16/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//

import UIKit
import RealmSwift

class Student: Object {
    public dynamic var name: String?
    public dynamic var id: String?
    public dynamic var contactNumber: String?
    public dynamic var schoolId: String?
    
    convenience init(name: String?,
         id: String?,
         contactNumber: String?,
         schoolId: String?) {
        self.init()
        self.name = name
        self.id = id
        self.contactNumber = contactNumber
        self.schoolId = schoolId
    }
}
