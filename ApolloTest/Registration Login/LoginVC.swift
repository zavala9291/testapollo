//
//  LoginVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 9/26/17.
//  Copyright © 2017 Ismael Zavala. All rights reserved.
//


import Foundation
import UIKit
import Firebase
import RealmSwift

internal protocol LoginVCDelegate: class {
    func registerButtonClicked(_loginVC: LoginVC)
    func login(_loginVC: LoginVC)
}

class LoginVC: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    weak var delegate: LoginVCDelegate?
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var signInButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    fileprivate let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        self.passwordField.delegate = self
        self.title = "Login"
        self.scrollView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: self.view.frame.size.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.passwordField.resignFirstResponder()
        self.emailField.resignFirstResponder()
    }
    
    @IBAction func registerButtonAction(_ sender: Any) {
        delegate?.registerButtonClicked(_loginVC: self)
    }
    
    @IBAction func signInButtonAction(_ sender: Any) {
        self.getCredentialsLogin()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.getCredentialsLogin()
        return true
    }
    
    func getCredentialsLogin() {
        guard let email =  emailField.text, !email.isEmpty else {
            self.showCredentialsAlert()
            return
        }
        
        guard let password = passwordField.text, !password.isEmpty else {
            self.showCredentialsAlert()
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            log.debug("signed in as user: \(String(describing: user)) and error \(String(describing: error))")
            
            if let _ = error {
                let alert = UIAlertController.init(title: "Login Failed", message: "Please check network connection or credentials.", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: false, completion: nil)
            } else {
                //delete all old database data, new user can have different stuff
                let realm = try! Realm()
                try! realm.write {
                    realm.deleteAll()
                }
                
                //let coordinator of login
                self.delegate?.login(_loginVC: self)
            }
        }
    }
    
    func showCredentialsAlert() {
        let alert = UIAlertController.init(title: "Incorrect Credentials", message: "Email/Password combination not valid.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: false, completion: nil)
    }
}
