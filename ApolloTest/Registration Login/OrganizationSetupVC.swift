//
//  EmployeeParentDivisionVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 4/1/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase

internal protocol OrganizationSetupVCDelegate: class {
    func joinButtonClicked(_orgVC: OrganizationSetupVC)
    func createButtonClicked(_orgVC: OrganizationSetupVC)
}

class OrganizationSetupVC: UIViewController
{
    weak var delegate: OrganizationSetupVCDelegate?
    
    let realm = try! Realm()
    fileprivate var ref: DatabaseReference!
    var firstName = ""
    var lastName = ""
    
    @IBOutlet var organizationNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        ref = Database.database().reference()
        log.debug("realm is: \(String(describing: Realm.Configuration.defaultConfiguration.fileURL))")
    }
    
    @IBAction func newOrganizationAction(_ sender: Any) {
        //check if there are no organizations that exist with the given name
        //if yes show warning that a new name must be
        
        let newDBId = UUID.init().uuidString
        guard let newBDName = self.organizationNameTextField.text, !newBDName.isEmpty else {
            showAlert(message: "Please enter valid organization name.", title: "Invalid Name")
            return
        }
        
        guard let user = Auth.auth().currentUser else {
            log.error("unable to get current user")
            return
        }
        
        let newDB = DBInfo.init(currentDBName: newBDName, currentDBId: newDBId)
        
        try! self.realm.write {
            self.realm.add(newDB)
        }

        let newDBData = [
            UIDefinitions.allCheckIns: "futureCheckins",
            UIDefinitions.allSchools: "futureSchools",
            UIDefinitions.allStudents: "futureStudents",
            UIDefinitions.allUsers: "futureUsers",
            UIDefinitions.databaseInfo: "futureDatabaseInfo"
        ]
        
        let newMasterUser = [
            UIDefinitions.databaseId: newDBId,
            UIDefinitions.approved: "true",
            UIDefinitions.role: "owner",
            UIDefinitions.name: "\(firstName) \(lastName)",
            UIDefinitions.id: user.uid,
            UIDefinitions.databaseName: newBDName
        ]
        
        let masterUserBranch = [
            user.uid: newMasterUser
        ]
        
        let dbInfoDict = [
            UIDefinitions.id: newDBId,
            UIDefinitions.users: masterUserBranch
            ] as [String : Any]
        
        self.ref.child(UIDefinitions.allDatabases).child(newDBId).setValue(newDBData)
        self.ref.child(UIDefinitions.allUsers).child(user.uid).setValue(newMasterUser)
        self.ref.child(UIDefinitions.allDatabaseInfo).child(newBDName).setValue(dbInfoDict)
        
        DispatchQueue.main.async {
            //let coordinator know that flow is completed
            self.delegate?.createButtonClicked(_orgVC: self)
        }
    }
    
    @IBAction func joinOrganizationAction(_ sender: Any) {
        //search DB for the given DB name and if there's a match
        //add this user as potential user to the db
        
        guard let dbName = self.organizationNameTextField.text, !dbName.isEmpty else {
            showAlert(message: "Please enter valid organization name.", title: "Invalid Name")
            return
        }

        ref.child("AllDatabaseInfo").child(dbName).observeSingleEvent(of: .value, with: { (snapshot) in
            weak var weakSelf = self
            guard let fetchedDictionary = snapshot.value as? NSDictionary else {
                 weakSelf?.showAlert(message: "Please enter existing organization name or create new organization with given name.", title: "Organization name doesn't exist")
                return
            }
            weakSelf?.joinDatabase(dbDictionary: fetchedDictionary, dbName: dbName)
            DispatchQueue.main.async {
                //let coordinator know that flow is completed
                weakSelf?.delegate?.joinButtonClicked(_orgVC: self)
            }
        })
        
        
    }
    
    func joinDatabase(dbDictionary: NSDictionary, dbName: String) {
        //add this user to the requested db it's trying to join in AllDatabases
        //add this user to AllDatabaseInfo
        
        guard let dbId = dbDictionary["id"] as? String else {
            return
        }
        
        guard let user = Auth.auth().currentUser else {
            return
        }
        
        //update all instances of user info
        ref.child("AllDatabaseInfo").child(dbName).child("users").child(user.uid).setValue([UIDefinitions.approved: false, UIDefinitions.role: "", UIDefinitions.firstName: firstName, UIDefinitions.lastName: lastName, UIDefinitions.databaseId: dbId, UIDefinitions.databaseName: dbName, UIDefinitions.name: "\(firstName) \(lastName)"])
        ref.child("AllDatabases").child(dbId).child("AllUsers").child(user.uid).setValue([UIDefinitions.approved: false, UIDefinitions.role: "", UIDefinitions.firstName: firstName, UIDefinitions.lastName: lastName, UIDefinitions.databaseId: dbId, UIDefinitions.name: "\(firstName) \(lastName)"])
        
        ref.child("AllUsers").child(user.uid).child(UIDefinitions.databaseId).setValue(dbId)
        ref.child("AllUsers").child(user.uid).child(UIDefinitions.approved).setValue(false)
        ref.child("AllUsers").child(user.uid).child(UIDefinitions.name).setValue("\(firstName) \(lastName)")
        ref.child("AllUsers").child(user.uid).child(UIDefinitions.databaseName).setValue(dbName)
    }
    
    func showAlert(message: String, title: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        let action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        
        self.present(alert, animated: false, completion: nil)
    }
}
