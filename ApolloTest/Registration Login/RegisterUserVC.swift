//
//  RegisterUserVC.swift
//  ApolloTest
//
//  Created by Ismael Zavala on 1/2/18.
//  Copyright © 2018 Ismael Zavala. All rights reserved.
//

import Foundation
import UIKit
import Firebase

internal protocol RegisterUserVCDelegate: class {
    func createAccountButtonClicked(_loginVC: RegisterUserVC)
    
    //fired when the user taps the back button on the nav controller
    func didTapBackButton(_ withVC: RegisterUserVC)
}

class RegisterUserVC: UIViewController {
     weak var delegate: RegisterUserVCDelegate?
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var reEnteredPasswordTextField: UITextField!
    
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    var ref: DatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isMovingFromParentViewController {
            delegate?.didTapBackButton(self)
        }
    }

    @IBAction func registerButtonAction(_ sender: Any) {
        guard self.checkInputFields() else {
            log.error("issue checking input fields")
            return
        }
        
        let emailEntered = self.emailTextField.text ?? ""
        let passwordEntered = self.passwordTextField.text ?? ""
        
        Auth.auth().createUser(withEmail: emailEntered, password: passwordEntered) { (user, error) in
            if error != nil {
                log.error("error: creating user, with error: \(error?.localizedDescription)")
                
                if error?.localizedDescription == "The email address is already in use by another account." {
                    self.showAlert(message: "Please try again", title: "Account already created for user.")
                    return
                    
                } else if error?.localizedDescription == "The password must be 6 characters long or more." {
                    self.showAlert(message: "Please enter valid password", title: "Password length too short")
                    return
                }
                
                self.showAlert(message: "Please try again", title: "Unable to register account")
                return
            }
            //successfully created user
            //must add uid to DB to handle permissions of this user
            
            Auth.auth().signIn(withEmail: emailEntered, password: passwordEntered, completion: { (dataResult, signInError) in
                log.debug("signed in as user: \(String(describing: user)) and error \(String(describing: error))")
                
                guard let signedInUSer = dataResult?.user else {
                    log.error("unable to sign in")
                    return
                }
                
                if signInError != nil {
                    log.error("signing in")
                    return
                }
                
                guard let fetchedUser = dataResult?.user else {
                    log.error("unable to get user")
                    return
                }
                
                let uid = fetchedUser.uid
                
                let firstName = self.firstNameTextField.text ?? ""
                let lastName = self.lastNameTextField.text ?? ""
                
                //verifying email
                fetchedUser.sendEmailVerification(completion: nil)
        
                self.ref = Database.database().reference()
                self.ref.child("AllUsers").child(uid).setValue([UIDefinitions.approved:false, UIDefinitions.role:"", UIDefinitions.firstName:firstName, UIDefinitions.lastName:lastName, UIDefinitions.databaseId:""], withCompletionBlock: { (setError, reference) in
                    log.error("the error for setting value is: \(setError)")
                })
                
                DispatchQueue.main.async {
                    //delegate let know if button press
                    self.delegate?.createAccountButtonClicked(_loginVC: self)
                }
            })
        }
    }
    
    func checkInputFields() -> Bool {
        guard let firstName = firstNameTextField.text, !firstName.isEmpty else {
            showAlert(message: "Please enter first name", title: "Empty First Name")
            return false
        }
        
        guard let lastName = lastNameTextField.text, !lastName.isEmpty else {
            showAlert(message: "Please enter last name", title: "Empty Last Name")
            return false
        }
        
        guard let emailEntered = emailTextField.text, !emailEntered.isEmpty else {
            showAlert(message: "Please enter valid email to register account", title: "Invalid email")
            return false
        }
        
        guard let passwordEntered = passwordTextField.text, !passwordEntered.isEmpty else {
            showAlert(message: "Please enter valid password to register account", title: "Invalid password")
            return false
        }
        
        guard let reEnteredPasswordEntered = reEnteredPasswordTextField.text else {
            showAlert(message: "Please enter valid password to register account", title: "Invalid password")
            return false
        }
        
        guard reEnteredPasswordEntered == passwordEntered else {
            showAlert(message: "Passwords do not match", title: "Invalid password")
            return false
        }
        return true
    }
    
    func showAlert(message: String, title: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        let action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        
        self.present(alert, animated: false, completion: nil)
    }
}
